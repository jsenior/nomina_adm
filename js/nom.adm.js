function document_ready(){
    //Llamada a la Consulta cuando se preciona el objeto con el id = btnConsultar el codigo html
    $("#btnConsultar").click(btnConsultar_click);  
    $("form input,form select,form textarea").change(input_change);

}
    function btnConsultar_click(){
        if(!estaCompleto()){
            return;
        }
        
        bloquearTeclado();
        mostrarSpinner("<b>Consultando la Base de Datos<br>Espere por favor</b>");
           
        var data=$("#formularioConsulta").serialize();
        var vista="/repnc/Rep_controller/consulta_nom";
        
        console.info(data);

        $.post(vista,data,function(respuesta,status,xhr){
            ocultarSpinner();
            desbloquearTeclado();
            
            if(status==="success"){
               $("#previsualizar").html(respuesta);
               $("#filtro").keyup(filtro_keyup);
               var rem=16;//1 rem = 16 px
               var finalPos=$("#previsualizar").offset().top-$("#Encabezado").outerHeight()-(0.5*rem);
               $('html').animate({scrollTop:finalPos},"slow");
            }else{
               alert("No se pudo obtener los datos desde el servidor.");
            } 
        });
    }

    function bloquearTeclado(){
        $("*").unbind("keydown");/*eliminar manejadores previos*/
        $("*").keydown( /*asignar manejador bloqueador*/
            function(event){
                event.preventDefault();/*anula el evento de presionar la tecla*/
            }    
        );
    }

    function mostrarSpinner(mensajeHTML="<b>Espere por favor</b>"){
        $("#modal-spinner-mensaje").html(mensajeHTML);
        $('#modal-spinner').css('display','flex'); 
    }

    function ocultarSpinner(){
        $('#modal-spinner').css('display','none'); 
    }

    function desbloquearTeclado(){
        $("*").unbind("keydown",bloqueo_keydown);
    }

    function estaCompleto(){
        var mensaje="";
        
        var checkstipnom=$("#grupotipnom").find('input[type="checkbox"]');
        //console.log(checkstipnom);
        var vacio=true;
        for(var i=0;i<checkstipnom.length;i++){
            
            if($(checkstipnom[i]).prop("checked")===true){
                vacio=false;
                break;
            }
        }
        if(vacio){
            mensaje=addLine(mensaje,"   • Al menos un Tipo de nómina"); 
            $("#grupotipnom").parent().addClass("border-danger");
        }else{
            $("#grupotipnom").parent().removeClass("border-danger");
        }
        
        /* if($("#procesos").val()==""){
            mensaje=addLine(mensaje,"   • Al menos un proceso");     
            $("#procesos").addClass("border-danger");
        }else{
            $("#procesos").removeClass("border-danger");
        } */
        
        /* if($("#ano").val()==""){
            mensaje=addLine(mensaje,"   • Año");  
            $("#ano").addClass("border-danger");
        }else{
            $("#ano").removeClass("border-danger");
        } */
        
        /* if($("#desdemes").val()==""){
            mensaje=addLine(mensaje,"   • Desde Mes");     
            $("#desdemes").addClass("border-danger");
        }else{
            $("#desdemes").removeClass("border-danger");
        }
        if($("#hastames").val()==""){
            mensaje=addLine(mensaje,"   • Hasta Mes");     
            $("#hastames").addClass("border-danger");
        }else{
            $("#hastames").removeClass("border-danger");
        } */
        
        
        /* if($("#conceptos").val()==""){
            mensaje=addLine(mensaje,"   • Al menos un concepto");     
            $("#conceptos").addClass("border-danger");
        }else{
            $("#conceptos").removeClass("border-danger");
        } */
        /* if($("#poronapre").val()==""){
            mensaje=addLine(mensaje,"   • % ONAPRE");     
            $("#poronapre").addClass("border-danger");
        }else{
            $("#poronapre").removeClass("border-danger");
        } */
        /* if($("#pordianca").val()==""){
            mensaje=addLine(mensaje,"   • % DIANCA");     
            $("#pordianca").addClass("border-danger");
        }else{
            $("#pordianca").removeClass("border-danger");
        } */
        if(mensaje!=""){
            //alert("Faltan los siguientes valores:\r\n"+mensaje);
            msgbox("Faltan los siguientes valores:<br>"+mensaje);
            return false;
        }
        return true;
    }
    function appendMsgbox(titulo, icono, mensaje,botones){
        $("body").append('<div class="modal" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"></div>')
        $('<div class="modal-dialog modal-dialog-centered"></div>').appendTo('#staticBackdrop');
        $('<div class="modal-content bg-dark bg-gradient rounded-10 border border-secondary  shadow"></div>').appendTo('.modal-dialog');
        $('<div class="modal-header bg-dark bg-gradient text-orange rounded-10 border-0" ></div>').appendTo('.modal-content');
        /*aqui va el titulo*/ 
        $('<h5 class="modal-title" id="staticBackdropLabel">'+titulo+'</h5>').appendTo('.modal-header');
        $('<button type="button" class="btn btn-dark text-orange border border-secondary  d-flex justify-content-center align-items-center" onclick="msgboxDialogResult = \'CANCELAR\'; modal.hide();"><i class="fa fa-times fa-fw"></i></button>').appendTo('.modal-header');
        $('<div class="modal-body bg-dark bg-gradient text-white" ></div>').appendTo('.modal-content');
        $('<div class="row"></div>').appendTo('.modal-body');
        switch(icono.toUpperCase()){
            case 'ERROR':
                icono='fad fa-times-circle fa-3x'; break;
            case 'PREGUNTA':
                icono='fad fa-question-circle fa-3x'; break;
            case 'ADVERTENCIA':
                icono='fad fa-exclamation-circle fa-3x'; break;
            case 'INFORMACION':
                icono='fad fa-info-circle fa-3x'; break;
            case 'REALIZADO':
                icono='fad fa-check-circle fa-3x'; break;
            case 'PROHIBIDO':
                icono='fad fa-lock-alt fa-3x'; break;
            default:
                icono='fad fa-info-circle fa-3x'; break;
        }
        /*aqui va el icono en la clase*/
        $('<div class="col-auto m-1"><i class="'+icono+'"></i></div>').appendTo(".modal-body .row");
        /*aqui va el mensaje*/
        $('<div class="col m-1 align-items-center d-flex"><div class="p-0 m-0">'+mensaje+'</div></div>').appendTo(".modal-body .row");
        $('<div class="modal-footer justify-content-center bg-dark bg-gradient rounded-10 border-0"></div>').appendTo('.modal-content');
        var botonesArray=botones.split(',');
        for(var i=0;i<botonesArray.length; i++){
            switch (botonesArray[i]) {
                case 'ACEPTAR':
                    $('<button autofocus type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'ACEPTAR\'; modal.hide();" id="msgbox_btn_ACEPTAR" ><i class="fad fa-check me-2"></i>Aceptar</button>').appendTo('.modal-footer');
                    break;
                case 'CANCELAR':
                    $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'CANCELAR\';  modal.hide();" id="msgbox_btn_CANCELAR"><i class="fad fa-times me-2"></i>Cancelar</button>').appendTo('.modal-footer');
                    break;
                case 'SI':
                    $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'SI\';  modal.hide();" id="msgbox_btn_SI"><i class="fad fa-check me-2"></i>Sí</button>').appendTo('.modal-footer');
                    break;
                case 'NO':
                    $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'NO\';  modal.hide();" id="msgbox_btn_NO"><i class="fad fa-times me-2"></i>No</button>').appendTo('.modal-footer');
                    break;
                default:
                    $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'ACEPTAR\';  modal.hide();" id="msgbox_btn_ACEPTAR"><i class="fad fa-check me-2"></i>Aceptar</button>').appendTo('.modal-footer');
            }
        }
        
    }
    async function msgbox(mensaje,titulo='Información',icono='INFORMACION',botones='ACEPTAR',enfocado='ACEPTAR',callback=null){
        let controlActivo=document.activeElement;
        let myPromise = new Promise(function (myResolve, myReject) {
               
                msgboxDialogResult="ESPERANDO";
                
                $(".modal-backdrop").remove();//ocultar fondo translucido en caso de que se llamen 2 msgbox seguidos
                $("#staticBackdrop").remove();
    
                //$('body').append(texto);
                appendMsgbox(titulo,icono,mensaje,botones);
    
                modal=new bootstrap.Modal($("#staticBackdrop"),{undefined,keyboard:true,focus:true});
                modal.show();                            
                //modal.toggle();
                $(".modal-dialog").hide();
                $(".modal-dialog").fadeIn("fast",function(){                
                                    $("#msgbox_btn_"+enfocado).focus();                    
                            });
                var timer=setInterval(checkearResultado,1);
                function checkearResultado(){
                    if(msgboxDialogResult!=='ESPERANDO'){
                        clearInterval(timer);
                        myResolve(msgboxDialogResult);
                    }
                }    
                //ENFOCAR EL BOTON PREDETERMINADO
                setTimeout(function(){
                    $("#msgbox_btn_"+enfocado).focus();  
    
                },100); 
                
        });    
        var resultado=await myPromise;
        $("#staticBackdrop").remove();//quitar el dialogo oculto que queda luego de cerrarlo
        $("body").removeAttr("class").removeAttr("style").removeAttr("data-bs-padding-right").removeAttr("data-bs-overflow");//quitar atributos que quedan luego de cerrar el dialogo
        $(controlActivo).focus();
        return resultado;
    }

    function bloqueo_keydown(event){
        event.preventDefault();
    }

    function addLine(cadena,linea){
        if(cadena==""){
            cadena+=linea;
        }else{
            cadena+="<br>"+linea;
        }        
        return cadena;
    }

    function input_change(){
        $("#previsualizar").html("");/*BORRA LA CONSULTA AL CAMBIAR LOS PARAMETROS PARA QUE EL USUARIO NO PUEDA DESCARGAR LA CONSULTA EQUIVOCADA*/
        var input=$(this);
        var inputId=input.prop("id");
        var inputType=input.prop("type");
        //SI EL CAMPO ESTA VACIO
    //    if(input.val()==""){
    //        input.addClass("border-danger");
    //    }else{
    //        input.removeClass("border-danger");
    //        //input.addClass("is-valid");
    //    }
        if(inputType==="date"){
            //VALIDACION DE MINIMOS Y MAXIMOS EN FECHAS
            var fechaval=new Date(input.val()+"T00:00:00");//se agrega "T00:00:00" para que no reste la zona horaria y la fecha se cree correctamente si no, resta un dia al restar la zona horaria
            var fechamax=new Date(input.prop("max")+"T00:00:00");
            var fechamin=new Date(input.prop("min")+"T00:00:00");
            
            if(fechaval>fechamax && input.val()!==""){
                input.val(input.prop("max"));
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                msgbox("La fecha máxima no puede ser mayor a:<br>"+fechamax.toLocaleDateString("es-VE",options));
            }
            if(fechaval<fechamin && input.val()!==""){
                input.val(input.prop("min"));
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                msgbox("La fecha mínima no puede ser menor a:<br>"+fechamin.toLocaleDateString("es-VE",options));
            }
            //VALIDACION DE INPUTS TIPO DATE MINIMOS Y MAXIMOS ASOCIADOS
            if(input.attr("inputMax")){
                if(input.val()!==""){
                    var fecha=new Date(input.val()+"T00:00:00");
                    var fechamax;
                    if($("#"+input.attr("inputMax")).val()!==""){
                        fechamax=new Date($("#"+input.attr("inputMax")).val()+"T00:00:00");
                    }else{//input.prop("min")
                        fechamax=new Date($("#"+input.attr("inputMax")).prop("min")+"T00:00:00");
                    }                
                    if(fecha>fechamax){
                        $("#"+input.attr("inputMax")).val(input.val());
                    }
                }else{
                    $("#"+input.attr("inputMax")).val("");
                }
            }
            if(input.attr("inputMin")){
                if(input.val()!==""){
                    var fecha=new Date(input.val()+"T00:00:00");
                    var fechamin;
                    if($("#"+input.attr("inputMin")).val()!==""){
                        fechamin=new Date($("#"+input.attr("inputMin")).val()+"T00:00:00");
                    }else{//input.prop("min")
                        fechamin=new Date($("#"+input.attr("inputMin")).prop("max")+"T00:00:00");
                    }  
                    if(fecha<fechamin){
                        $("#"+input.attr("inputMin")).val(input.val());
                    }else{
                        $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).val());
                        var fechaval=new Date($("#"+input.attr("inputMin")).val()+"T00:00:00");//se agrega "T00:00:00" para que no reste la zona horaria y la fecha se cree correctamente si no, resta un dia al restar la zona horaria
                        var fechamax=new Date($("#"+input.attr("inputMin")).prop("max")+"T00:00:00");
                        var fechamin=new Date($("#"+input.attr("inputMin")).prop("min")+"T00:00:00");
                        if(fechaval>fechamax){
                            $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("max"));
                        }
                        if(fechaval<fechamin){            
                            $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("min"));
                        }
                    }
                }else{
                    $("#"+input.attr("inputMin")).val("");
                }
            }
        }
        if(inputType=="number"){
            //VALIDACION DE MINIMOS Y MAXIMOS
            if(input.val()*1>input.prop("max")*1 && input.val()!==""){
                input.val(input.prop("max"));
            }
            if(input.val()*1<input.prop("min")*1 && input.val()!==""){            
                input.val(input.prop("min"));
            }
            
            //VALIDACION DE INPUTS MINIMOS Y MAXIMOS ASOCIADOS
            if(input.attr("inputMax")){
                if(input.val()!==""){
                    if(input.val()*1>$("#"+input.attr("inputMax")).val()*1){
                        $("#"+input.attr("inputMax")).val(input.val());
                    }
                }else{
                    $("#"+input.attr("inputMax")).val("");
                }
            }
            
            if(input.attr("inputMin")){
                if(input.val()!==""){
                    if(input.val()*1<$("#"+input.attr("inputMin")).val()*1){
                        $("#"+input.attr("inputMin")).val(input.val());
                    }else{
                        $("#"+input.attr("inputMin")).val(1*$("#"+input.attr("inputMin")).val());//por si es "" se convierta a 0
                        if($("#"+input.attr("inputMin")).val()*1>$("#"+input.attr("inputMin")).prop("max")*1){
                            $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("max"));
                        }
                        if($("#"+input.attr("inputMin")).val()*1<$("#"+input.attr("inputMin")).prop("min")*1){            
                            $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("min"));
                        }
                    }  
                }else{
                    $("#"+input.attr("inputMin")).val("");
                }      
            }
            //VALIDACION DE INPUTS BALANCEADOS
            if(input.attr("inputbalance")){            
                if(((input.prop("max")*1.00)-(input.val()*1.00))!=$("#"+input.attr("inputbalance")).val()){
                    $("#"+input.attr("inputbalance")).val(redondear((input.prop("max")*1.00)-(input.val()*1.00)))
                    //alert("entro " + );
                }
            }
        }
        
        //SI ES EL CHECK TODAS        
        if(inputId==="todostipnom"){    
            $("#grupotipnom input[type='checkbox']").prop("checked",input.prop("checked"));
        }else{
            var checks=$("#grupotipnom input[type='checkbox'][id!=todostipnom]");
            var todasChecked=true;
            for(var i=0;i<checks.length; i++){
                if(!$(checks[i]).prop("checked")){
                    todasChecked=false;
                    break;
                }  
            }
            $("#todostipnom").prop("checked",todasChecked);
        }        
    }
    
    function filtro_keyup(event){
        //asigna el evento de soltar una tecla en el textbox filtro
       if($(this).val()===""){
           
           if(event.key!=="Enter" && event.key!=="Escape" && event.key!=="Backspace" && event.key!=="Delete"){
               $("#tblConsulta tbody").removeHighlight();
               return;
           }
           //REPAGINAR SI QUEDA VACIO EL FILTRO
           $('#myPager').html("");
           $('#cuerpoTabla').pageMe(
                                       {
                                           pagerSelector:'#myPager',
                                           showPrevNext:true,
                                           showFirstLast:true,
                                           hidePageNumbers:true,
                                           perPage:14                              
                                       }       
                                   );
           activarToolTips();
       } 
       var valor=$(this).val().toLowerCase();                
       $("#tblConsulta tbody tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(valor) > -1);        
       });   
       //resaltar usando plugin jquery jquery.highlight-5.js
       $("#tblConsulta tbody").removeHighlight();
       $("#tblConsulta tbody").highlight(valor);
       if(valor!==""){
           //REPAGINAR DESPUES DEL FILTRO
           $('#myPager').html("");
           $('#cuerpoTabla').pageMe(
                                       {
                                           pagerSelector:'#myPager',
                                           showPrevNext:true,
                                           showFirstLast:true,
                                           hidePageNumbers:true,
                                           perPage:14,
                                           filter:valor                                  
                                       }       
                                   );
           activarToolTips();
       }
   }
   

$(document).ready(document_ready);
