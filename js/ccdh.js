function document_ready(){  
    //$("button").mouseup(button_mouseup);
    $("input[type=number]").keydown(solo_numeros_keydown);
    $("form input,form select,form textarea").change(input_change);
    $("#btnConsultar").click(btnConsultar_click);    
    $("#btnSalir").click(btnSalir_click);    
    $("#conceptos").blur(conceptos_blur);
    $("#procesos").blur(procesos_blur);  
    $("#fichas").blur(fichas_blur);
    $("#talleres").blur(talleres_blur);
    $("#grupoconceptos").change(grupoconceptos_change);
    $("#grupoconceptos").change();
    //$("#grupoconceptos").change(grupoconceptos_change);
    //$("#grupoconceptos").trigger("change");    
    activarToolTips();
}
//function activarToolTips(container=null){
//    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
//    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
//                                                if(container===null){
//                                                    return new bootstrap.Tooltip(tooltipTriggerEl);
//                                                }else{
//                                                    return new bootstrap.Tooltip(tooltipTriggerEl,{container:container});
//                                                }
//                                            });
//}
function button_mouseup(){    
    //$(this).find("i").stop("parpadeo-latido");
    $(this).find("i").removeClass("clickear");
    $(this).find("i").addClass("clickear");
}
function filtrarNombre(cadena=""){
    var nombre="";
    
    cadena=cadena.trim();
    
    while(cadena.indexOf("  ")!==-1){
        cadena=cadena.replace(/  /g," ");
    }   
    cadena=cadena.replace(/[^A-Za-záéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ' ]/g,"");
    subcads=cadena.split(" ");
    
    for(var i=0; i<subcads.length;i++){
        subcads[i]=subcads[i].charAt(0).toUpperCase()+subcads[i].slice(1).toLowerCase();
    }
    
    nombre=subcads.join(" ");
    
    return nombre;
}
function filtrarConceptos(conceptos=""){
    let resultado="";
    while(conceptos.charAt(0)==="-"){//eliminar los - del principio
        conceptos=conceptos.substr(1);
    }
    while(conceptos.charAt(conceptos.length-1)==="-"){//eliminar los - del final
        conceptos=conceptos.substr(0,conceptos.length-1);
    }
    conceptos=conceptos.replace(/( +)/g," ");//cambia 1 o mas espacios por uno solo 
    conceptos=conceptos.replace(/(-+)/g,"-");//cambia 1 o mas - por uno solo 
    conceptos=conceptos.replace(/(\++)/g,"+");//cambia 1 o mas + por uno solo 
    conceptos=conceptos.replace(/( +)(-)|(-)( +)/g,"-");//cambia 1 o mas espacios por uno solo
    conceptos=conceptos.replace(/[('')("")(\/)(\|)(\+).,; (\r\n)|(\r)|(\n)|(\t)]/g,",");
    conceptos=conceptos.replace(/(,+)/g,",");//cambia 1 o mas , por uno solo
    conceptos=conceptos.replace(/[^0-9,-]/g,"");
    conceptos=conceptos.replace(/(,-)|(-,)/g,"-"); 
    conceptos=conceptos.replace(/(-+)/g,"-");//cambia 1 o mas - por uno solo 
    while(conceptos.search(/(-)[0-9]{1,3}(-)/g)!==-1){//mientras el patron sea encontrado
        conceptos=conceptos.replace(/(-)[0-9]{1,3}(-)/g,"-");//cambia 1 o mas -xxx- por - 
    }
    
    let conceptosArray=conceptos.split(",");
    for(let i=0; i<conceptosArray.length; i++){
        if(resultado===""){
            if(conceptosArray[i]!==""){                
                resultado=conceptosArray[i];
            }
        }else{
            if(conceptosArray[i]!==""){
                resultado+=", "+conceptosArray[i];                
            }
        }
    }
    
    
    return resultado;
}
function filtrarFichas(fichas=""){
    let resultado="";
    fichas=fichas.toUpperCase();
    fichas=fichas.replace(/[('')("")(\/)(\|).,; (\r\n)|(\r)|(\n)|(\t)]/g,",");    
    fichas=fichas.replace(/[^A-Z0-9(,)(+)(\-)]/g,"");    
    let fichasArray=fichas.split(",");
    for(let i=0; i<fichasArray.length; i++){
        if(resultado===""){
            if(fichasArray[i]!==""){
                resultado="'"+fichasArray[i]+"'";
            }
        }else{
            if(fichasArray[i]!==""){
                resultado+=", '"+fichasArray[i]+"'";
            }
        }
    }
    return resultado;
}
function filtrarTalleres(talleres=""){
    let resultado="";
    talleres=talleres.replace(/[('')("")(\/)(\|)(-).,; (\r\n)|(\r)|(\n)|(\t)]/g,",");
    talleres=talleres.replace(/[^0-9,]/g,"");
    let talleresArray=talleres.split(",");
    for(let i=0; i<talleresArray.length; i++){
        if(resultado===""){
            if(talleresArray[i]!==""){
                resultado="'"+talleresArray[i]+"'";
            }
        }else{
            if(talleresArray[i]!==""){
                resultado+=", '"+talleresArray[i]+"'";
            }
        }
    }
    return resultado;
}
function solo_numeros_keydown(event){
    //alert(event.key);
    switch(event.key){
        case "0":case "1":case "2":
        case "3":case "4":case "5":
        case "6":case "7":case "8":case "9": 
        case "Backspace": case "Delete":case "Tab":
        case "ArrowRight": case "ArrowLeft":
        case "ArrowUp": case "ArrowDown":
            break;
        case ".":
            event.key=",";
        case ",":                     
            if($(this).val().toString().indexOf(".")>-1){
                event.preventDefault();
            }
            break;
        default: event.preventDefault();
    }   
}
function grupoconceptos_change(){
    if($(this).val()==='1'){
        let conceptos="010, 011, 030, 031, 040, 041, 042, 060, 061, 070, 071, 072, 075, 076, 080, 081, 082, 090, 091, 092, 096, 097, 098, 099, 100, 101, 110, 111, 112, 113, 114, 118, 120, 121, 123, 140, 240, 241, 330, 331, 340, 341, 390, 390, 440, 441, 450, 451, 500, 501, 587, 588, 700, 703, 760, 770, 782, 790, 791, 792, 794, 801, 802, 803, 804, 805, 806, 808";
        $("#conceptos").val(conceptos);
    }else if($(this).val()==='2'){
        let conceptos="043, 160, 161, 170, 171, 180, 181, 190, 191, 288, 289, 312";
        $("#conceptos").val(conceptos);
    }else if($(this).val()==='3'){
        let conceptos="001, 002, 003, 004, 005, 006, 007, 230, 280";
        $("#conceptos").val(conceptos);
    }else if($(this).val()==='4'){
        let conceptos="";
        $("#conceptos").val(conceptos);
    }
}
function procesos_blur(){
    $(this).val(filtrarConceptos($(this).val()));
}
function conceptos_blur(){
    $(this).val(filtrarConceptos($(this).val()));
}
function fichas_blur(){
    $(this).val(filtrarFichas($(this).val()));
}
function talleres_blur(){
    $(this).val(filtrarTalleres($(this).val()));
}
function bloquearTeclado(){
    $("*").unbind("keydown");/*eliminar manejadores previos*/
    $("*").keydown( /*asignar manejador bloqueador*/
        function(event){
            event.preventDefault();/*anula el evento de presionar la tecla*/
        }    
    );
}
function desbloquearTeclado(){
    $("*").unbind("keydown");/*eliminar manejador bloqueador*/
    $("*").unbind("click");
    $("*").unbind("change");/*eliminar manejador click ¿POR QUÉ? LEER ABAJO LO QUE SIGUE*/
    /* OJO
     * antes de restablecer los manejadores originales se deben eliminar
     * todos los manejadores actuales para que no se dupliquen al reasignarlos
    */
    document_ready();/*restablecer los manejadores originales*/
}
function estaCompleto(){
    var mensaje="";
    
    var checkstipnom=$("#grupotipnom").find('input[type="checkbox"]');
    //console.log(checkstipnom);
    var vacio=true;
    for(var i=0;i<checkstipnom.length;i++){
        
        if($(checkstipnom[i]).prop("checked")===true){
            vacio=false;
            break;
        }
    }
    if(vacio){
        mensaje=addLine(mensaje,"   • Al menos un Tipo de nómina"); 
        $("#grupotipnom").parent().addClass("border-danger");
    }else{
        $("#grupotipnom").parent().removeClass("border-danger");
    }
    
    if($("#procesos").val()==""){
        mensaje=addLine(mensaje,"   • Al menos un proceso");     
        $("#procesos").addClass("border-danger");
    }else{
        $("#procesos").removeClass("border-danger");
    }
    
    if($("#ano").val()==""){
        mensaje=addLine(mensaje,"   • Año");  
        $("#ano").addClass("border-danger");
    }else{
        $("#ano").removeClass("border-danger");
    }
    
    if($("#desdemes").val()==""){
        mensaje=addLine(mensaje,"   • Desde Mes");     
        $("#desdemes").addClass("border-danger");
    }else{
        $("#desdemes").removeClass("border-danger");
    }
    if($("#hastames").val()==""){
        mensaje=addLine(mensaje,"   • Hasta Mes");     
        $("#hastames").addClass("border-danger");
    }else{
        $("#hastames").removeClass("border-danger");
    }
    
    
    if($("#conceptos").val()==""){
        mensaje=addLine(mensaje,"   • Al menos un concepto");     
        $("#conceptos").addClass("border-danger");
    }else{
        $("#conceptos").removeClass("border-danger");
    }
    if($("#poronapre").val()==""){
        mensaje=addLine(mensaje,"   • % ONAPRE");     
        $("#poronapre").addClass("border-danger");
    }else{
        $("#poronapre").removeClass("border-danger");
    }
    if($("#pordianca").val()==""){
        mensaje=addLine(mensaje,"   • % DIANCA");     
        $("#pordianca").addClass("border-danger");
    }else{
        $("#pordianca").removeClass("border-danger");
    }
    if(mensaje!=""){
        //alert("Faltan los siguientes valores:\r\n"+mensaje);
        msgbox("Faltan los siguientes valores:<br>"+mensaje);
        return false;
    }
    return true;
}
function addLine(cadena,linea){
    if(cadena==""){
        cadena+=linea;
    }else{
        cadena+="<br>"+linea;
    }        
    return cadena;
}
function input_change(){
    $("#previsualizar").html("");/*BORRA LA CONSULTA AL CAMBIAR LOS PARAMETROS PARA QUE EL USUARIO NO PUEDA DESCARGAR LA CONSULTA EQUIVOCADA*/
    var input=$(this);
    var inputId=input.prop("id");
    var inputType=input.prop("type");
    //SI EL CAMPO ESTA VACIO
//    if(input.val()==""){
//        input.addClass("border-danger");
//    }else{
//        input.removeClass("border-danger");
//        //input.addClass("is-valid");
//    }
    if(inputType==="date"){
        //VALIDACION DE MINIMOS Y MAXIMOS EN FECHAS
        var fechaval=new Date(input.val()+"T00:00:00");//se agrega "T00:00:00" para que no reste la zona horaria y la fecha se cree correctamente si no, resta un dia al restar la zona horaria
        var fechamax=new Date(input.prop("max")+"T00:00:00");
        var fechamin=new Date(input.prop("min")+"T00:00:00");
        
        if(fechaval>fechamax && input.val()!==""){
            input.val(input.prop("max"));
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            msgbox("La fecha máxima no puede ser mayor a:<br>"+fechamax.toLocaleDateString("es-VE",options));
        }
        if(fechaval<fechamin && input.val()!==""){
            input.val(input.prop("min"));
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            msgbox("La fecha mínima no puede ser menor a:<br>"+fechamin.toLocaleDateString("es-VE",options));
        }
        //VALIDACION DE INPUTS TIPO DATE MINIMOS Y MAXIMOS ASOCIADOS
        if(input.attr("inputMax")){
            if(input.val()!==""){
                var fecha=new Date(input.val()+"T00:00:00");
                var fechamax;
                if($("#"+input.attr("inputMax")).val()!==""){
                    fechamax=new Date($("#"+input.attr("inputMax")).val()+"T00:00:00");
                }else{//input.prop("min")
                    fechamax=new Date($("#"+input.attr("inputMax")).prop("min")+"T00:00:00");
                }                
                if(fecha>fechamax){
                    $("#"+input.attr("inputMax")).val(input.val());
                }
            }else{
                $("#"+input.attr("inputMax")).val("");
            }
        }
        if(input.attr("inputMin")){
            if(input.val()!==""){
                var fecha=new Date(input.val()+"T00:00:00");
                var fechamin;
                if($("#"+input.attr("inputMin")).val()!==""){
                    fechamin=new Date($("#"+input.attr("inputMin")).val()+"T00:00:00");
                }else{//input.prop("min")
                    fechamin=new Date($("#"+input.attr("inputMin")).prop("max")+"T00:00:00");
                }  
                if(fecha<fechamin){
                    $("#"+input.attr("inputMin")).val(input.val());
                }else{
                    $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).val());
                    var fechaval=new Date($("#"+input.attr("inputMin")).val()+"T00:00:00");//se agrega "T00:00:00" para que no reste la zona horaria y la fecha se cree correctamente si no, resta un dia al restar la zona horaria
                    var fechamax=new Date($("#"+input.attr("inputMin")).prop("max")+"T00:00:00");
                    var fechamin=new Date($("#"+input.attr("inputMin")).prop("min")+"T00:00:00");
                    if(fechaval>fechamax){
                        $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("max"));
                    }
                    if(fechaval<fechamin){            
                        $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("min"));
                    }
                }
            }else{
                $("#"+input.attr("inputMin")).val("");
            }
        }
    }
    if(inputType=="number"){
        //VALIDACION DE MINIMOS Y MAXIMOS
        if(input.val()*1>input.prop("max")*1 && input.val()!==""){
            input.val(input.prop("max"));
        }
        if(input.val()*1<input.prop("min")*1 && input.val()!==""){            
            input.val(input.prop("min"));
        }
        
        //VALIDACION DE INPUTS MINIMOS Y MAXIMOS ASOCIADOS
        if(input.attr("inputMax")){
            if(input.val()!==""){
                if(input.val()*1>$("#"+input.attr("inputMax")).val()*1){
                    $("#"+input.attr("inputMax")).val(input.val());
                }
            }else{
                $("#"+input.attr("inputMax")).val("");
            }
        }
        
        if(input.attr("inputMin")){
            if(input.val()!==""){
                if(input.val()*1<$("#"+input.attr("inputMin")).val()*1){
                    $("#"+input.attr("inputMin")).val(input.val());
                }else{
                    $("#"+input.attr("inputMin")).val(1*$("#"+input.attr("inputMin")).val());//por si es "" se convierta a 0
                    if($("#"+input.attr("inputMin")).val()*1>$("#"+input.attr("inputMin")).prop("max")*1){
                        $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("max"));
                    }
                    if($("#"+input.attr("inputMin")).val()*1<$("#"+input.attr("inputMin")).prop("min")*1){            
                        $("#"+input.attr("inputMin")).val($("#"+input.attr("inputMin")).prop("min"));
                    }
                }  
            }else{
                $("#"+input.attr("inputMin")).val("");
            }      
        }
        //VALIDACION DE INPUTS BALANCEADOS
        if(input.attr("inputbalance")){            
            if(((input.prop("max")*1.00)-(input.val()*1.00))!=$("#"+input.attr("inputbalance")).val()){
                $("#"+input.attr("inputbalance")).val(redondear((input.prop("max")*1.00)-(input.val()*1.00)))
                //alert("entro " + );
            }
        }
    }
    
    //SI ES EL CHECK TODAS        
    if(inputId==="todostipnom"){    
        $("#grupotipnom input[type='checkbox']").prop("checked",input.prop("checked"));
    }else{
        var checks=$("#grupotipnom input[type='checkbox'][id!=todostipnom]");
        var todasChecked=true;
        for(var i=0;i<checks.length; i++){
            if(!$(checks[i]).prop("checked")){
                todasChecked=false;
                break;
            }  
        }
        $("#todostipnom").prop("checked",todasChecked);
    }        
}
function redondear(numero,decimales=2){
    
    return Math.round((numero+Number.EPSILON)*100)/100;
}
function btnFullScreen_click(){
    var icono=$(this).find("i");
    icono.removeClass("fa-expand-arrows");
    icono.removeClass("fa-compress-arrows-alt");
    
    var elemento=document.getElementById("previsualizar");
    if (document.fullscreenElement){
        document.exitFullscreen();
        icono.addClass("fa-expand-arrows");
        $(this).prop("title","Ver en Pantalla Completa");
        activarToolTips();
        //MOSTRAR LA PAGINA ACTUAL DE DATOS
        $('.pager').find('#pagesSelect').change();        
    }else{
        elemento.requestFullscreen();
        icono.addClass("fa-compress-arrows-alt");
        $(this).prop("title","Salir de Pantalla Completa");
        activarToolTips("#previsualizar");//PARA QUE SE VEA EN PANTALLA COMPLETA
        //new bootstrap.Tooltip(this,{container:"#previsualizar"});//PARA QUE SE VEA EN PANTALLA COMPLETA
        //setTimeout(activarToolTips,33);
        
        //MOSTRAR TODAS LAS FILAS        
        $("#tblConsulta tbody tr").filter(function() {
            $(this).toggle(true);
        });  
    }
}
function btnSalir_click(){
    mostrarSpinner("<b>Cerrando sesion del módulo CCDH</b>");
    $.post('/CI3CCDH/ccdh/salir',function(){
        window.location.assign('/CI3CCDH');
    });
    
}
function btnDescargar_click(){
    mostrarSpinner("<b>Descargando CSV de Excel</b>");
    setTimeout(function(){
        var fechahora=new Date();
        var titulo=$("#tituloConsulta").text();
        fechahora=fechahora.getDate().toString().padStart(2,'0')+'-'+(fechahora.getMonth()+1).toString().padStart(2,'0')+'-'+fechahora.getFullYear().toString()+" "+fechahora.getHours().toString().padStart(2,'0')+'h'+fechahora.getMinutes().toString().padStart(2,'0')+'m'+fechahora.getSeconds().toString().padStart(2,'0')+'s';
        $.when(exportTableToCSV("tblConsulta",titulo+' - '+fechahora)).then(ocultarSpinner());
    },33);    
}
function btnDescargarTXT_click(){
    mostrarSpinner("<b>Descargando TXT de Notepad</b>");
    setTimeout(function(){
        var fechahora=new Date();
        var titulo=$("#tituloConsulta").text();
        fechahora=fechahora.getDate().toString().padStart(2,'0')+'-'+(fechahora.getMonth()+1).toString().padStart(2,'0')+'-'+fechahora.getFullYear().toString()+" "+fechahora.getHours().toString().padStart(2,'0')+'h'+fechahora.getMinutes().toString().padStart(2,'0')+'m'+fechahora.getSeconds().toString().padStart(2,'0')+'s';
        $.when(exportTableToTXT("tblConsulta",titulo+' - '+fechahora)).then(ocultarSpinner());
    },33);    
}
function btnDescargarXLSX_click(){
    /**/
    mostrarSpinner("<b>Descargando XLSX de Excel</b>");
    setTimeout(function(){
        var fechahora=new Date();    
    var titulo=$("#tituloConsulta").text();
    fechahora=fechahora.getFullYear().toString()+"-"+(fechahora.getMonth()+1).toString().padStart(2,'0')+"-"+fechahora.getDate().toString().padStart(2,'0')+" "+fechahora.getHours().toString().padStart(2,'0')+"h"+fechahora.getMinutes().toString().padStart(2,'0')+"m"+fechahora.getSeconds().toString().padStart(2,'0')+"s";
    var nombreArchivo=titulo+' '+fechahora;
    var tabla=document.getElementById('tblConsulta');//$('#tblConsulta');
    var tableExportConfig={
	headers: true, // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
	footers: true, // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
	formats: ["xlsx", "csv", "txt"], // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
	filename: nombreArchivo, // (id, String), filename for the downloaded file, (default: 'id')
	bootstrap: false, // (Boolean), style buttons using bootstrap, (default: true)
	exportButtons: false, // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
	position: "bottom", // (top, bottom), position of the caption element relative to table, (default: 'bottom')
	ignoreRows: null, // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
	ignoreCols: null, // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
	trimWhitespace: true, // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
	RTL: false, // (Boolean), set direction of the worksheet to right-to-left (default: false)
	sheetname: fechahora // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    };
//    {
//			exportButtons: false, // No queremos botones
//			filename: nombreArchivo, //Nombre del archivo de Excel
//			sheetname: nombreArchivo, //Título de la hoja
//		}
    var tableExport = TableExport(tabla, tableExportConfig);
    var exportData = tableExport.getExportData();
    //console.log(exportData);
    var xlsxData = exportData.tblConsulta.xlsx;
    tableExport.export2file(xlsxData.data, xlsxData.mimeType, xlsxData.filename, xlsxData.fileExtension, xlsxData.merges, xlsxData.RTL, xlsxData.sheetname);
    ocultarSpinner();
    },33);
    
}
function btnCopiar_click(){    
    copiarDatosTabla("tblConsulta");
}
function btnDescargarPDF_click(){
    mostrarSpinner("<b>Descargando PDF de Acrobat</b>");
    setTimeout(function(){
        //ARMAR NOMBRE DE ARCHIVO
        var fechahora=new Date();    
        var titulo=$("#tituloConsulta").text();
        fechahora=fechahora.getDate().toString().padStart(2,'0')+'-'+(fechahora.getMonth()+1).toString().padStart(2,'0')+'-'+fechahora.getFullYear().toString()+" "+fechahora.getHours().toString().padStart(2,'0')+'h'+fechahora.getMinutes().toString().padStart(2,'0')+'m'+fechahora.getSeconds().toString().padStart(2,'0')+'s';
        var nombreArchivo=titulo+' - '+fechahora;

        //MOSTRAR TODAS LAS FILAS        
        $("#tblConsulta tbody tr").filter(function() {
            $(this).toggle(true);
        });   

        //QUITAR EL RESALTADO DE TEXTO
        $("#tblConsulta tbody").removeHighlight();

        //OPTENER LA TABLA
        var tabla=document.getElementById("tblConsulta");

        //CREAR CONFIGURACION PARA html2pdf
        var opt = {
            margin:       [2,2,2,2],
            filename:     nombreArchivo+'.pdf',
            image:        { type: 'jpeg', quality: 0.98 },
            html2canvas:  { scale: 2, scrollX:0,scrollY:0 },//scale:1=96dpi, 2=192dpi...| los scroll en 0 para dibujar desde el comienzo de la pagina
            jsPDF:        { unit: 'cm', format: 'letter', orientation: 'portrait' }
        };       

        //CREAR PDF
        html2pdf().set(opt).from(tabla).save().then(function(){ocultarSpinner();});
        
    },33);
    
}
function btnConsultar_click(){
    if(!estaCompleto()){
        return;
    }
    
    bloquearTeclado();
    mostrarSpinner("<b>Consultando la Base de Datos<br>Espere por favor</b>");
       
    var data=$("#formularioConsulta").serialize();
    var vista="/CI3CCDH/ccdh/previsualizar";
    
    
    $.post(vista,data,function(respuesta,status,xhr){
        ocultarSpinner();
        desbloquearTeclado();
        
        if(status==="success"){
           $("#previsualizar").html(respuesta);
           $("#filtro").keyup(filtro_keyup);
           var rem=16;//1 rem = 16 px
           var finalPos=$("#previsualizar").offset().top-$("#Encabezado").outerHeight()-(0.5*rem);
           $('html').animate({scrollTop:finalPos},"slow");
        }else{
           alert("No se pudo obtener los datos desde el servidor.");
        } 
    });
}
function grupoconceptos_change(){
    const DeduccionesParaFiscales="700, 760, 770, 782, 790, 940";
    const DeduccionesNoParaFiscales="182, 472, 584, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 730, 731, 732, 733, 734, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 755, 761, 771, 772, 783, 784, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 817, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 849, 850, 851, 852, 853, 854, 855, 856, 857, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 910, 911, 912, 913, 914, 915, 916, 917, 918, 920, 921, 922, 930, 931, 932, 934, 935, 936, 937, 938, 941, 942, 943, 944, 945, 950, 951, 960, 961, 962, 963, 964";
    const DeduccionesTodas="182, 472, 584, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 730, 731, 732, 733, 734, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 755, 760, 761, 770, 771, 772, 782, 783, 784, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 817, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 849, 850, 851, 852, 853, 854, 855, 856, 857, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 910, 911, 912, 913, 914, 915, 916, 917, 918, 920, 921, 922, 930, 931, 932, 934, 935, 936, 937, 938, 940, 941, 942, 943, 944, 945, 950, 951, 960, 961, 962, 963, 964";
    const AsignacionesTodas="1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 29, 30, 31, 32, 33, 40, 41, 42, 43, 50, 51, 60, 61, 62, 70, 71, 72, 75, 76, 80, 81, 82, 90, 91, 92, 96, 97, 98, 99, 100, 101, 102, 105, 106, 107, 108, 110, 111, 112, 113, 114, 115, 118, 119, 120, 121, 122, 123, 124, 125, 126, 131, 132, 133, 134, 135, 140, 141, 143, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 170, 171, 180, 181, 190, 191, 195, 200, 210, 220, 230, 231, 232, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 280, 281, 282, 288, 289, 290, 291, 300, 301, 302, 310, 311, 312, 320, 321, 330, 331, 332, 335, 336, 340, 341, 342, 345, 346, 350, 351, 360, 361, 370, 371, 380, 381, 382, 383, 390, 391, 400, 401, 402, 410, 411, 412, 420, 421, 422, 423, 430, 431, 440, 441, 450, 451, 452, 453, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 530, 531, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 598, 599, 600, 601, 610, 611, 612, 613, 614, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 630, 631, 640, 641, 642, 643, 644, 645, 646, 647, 650, 651, 660, 661, 723, 730, 735, 740, 741, 761, 762, 773, 777, 784, 789, 816, 817, 818, 819, 824, 857, 867, 885, 937, 945, 951, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993";
    const AsignacionesNomina="1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 29, 30, 31, 32, 33, 40, 41, 42, 43, 50, 51, 60, 61, 62, 70, 71, 72, 75, 76, 80, 81, 82, 90, 91, 92, 96, 97, 98, 99, 100, 101, 102, 105, 106, 108, 110, 111, 112, 113, 114, 115, 118, 119, 120, 121, 122, 123, 124, 125, 126, 131, 132, 133, 134, 135, 140, 141, 143, 150, 151, 152, 153, 154, 155, 156, 157, 160, 161, 162, 170, 171, 180, 181, 190, 191, 195, 200, 210, 220, 230, 231, 240, 241, 242, 245, 246, 247, 248, 249, 250, 251, 260, 261, 262, 263, 264, 266, 267, 268, 270, 271, 272, 273, 275, 276, 277, 280, 281, 288, 289, 290, 291, 300, 301, 302, 310, 311, 312, 320, 321, 330, 331, 332, 335, 336, 340, 341, 342, 345, 346, 350, 351, 360, 361, 370, 371, 380, 381, 382, 383, 390, 391, 400, 401, 402, 410, 411, 412, 420, 421, 422, 423, 430, 431, 440, 441, 450, 451, 452, 453, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 530, 540, 541, 542, 543, 544, 545, 546, 547, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 571, 572, 573, 574, 575, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 590, 592, 600, 601, 610, 611, 612, 614, 616, 617, 618, 619, 620, 621, 622, 625, 626, 627, 630, 631, 640, 641, 642, 643, 650, 735, 740, 741, 761, 762, 773, 784, 817, 824, 867, 885, 980, 981, 982, 983, 984, 985, 988, 993";
    const AsignacionesAnticipo="240, 980, 981, 982, 983, 984, 985, 988";
    const AsignacionesNominaEspecial="1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 19, 20, 22, 23, 29, 30, 31, 32, 40, 41, 42, 43, 50, 51, 60, 61, 70, 71, 72, 75, 76, 80, 81, 82, 90, 91, 92, 96, 97, 98, 99, 100, 101, 102, 105, 106, 107, 108, 110, 111, 112, 113, 114, 115, 118, 119, 120, 121, 123, 124, 131, 132, 133, 134, 135, 140, 141, 143, 150, 151, 152, 153, 154, 155, 156, 157, 158, 160, 161, 162, 170, 171, 180, 181, 190, 191, 195, 220, 230, 231, 240, 241, 242, 243, 244, 246, 247, 248, 249, 250, 251, 252, 254, 260, 261, 263, 264, 265, 268, 270, 271, 272, 275, 276, 277, 280, 281, 289, 300, 301, 302, 311, 312, 320, 321, 330, 331, 332, 340, 341, 360, 361, 370, 420, 440, 441, 450, 451, 470, 480, 481, 500, 501, 520, 530, 531, 540, 541, 542, 543, 544, 545, 546, 547, 558, 560, 561, 562, 563, 564, 565, 567, 569, 571, 572, 573, 574, 575, 576, 577, 578, 580, 581, 582, 584, 585, 587, 588, 590, 591, 592, 593, 594, 598, 599, 600, 601, 610, 611, 612, 614, 616, 617, 618, 619, 622, 626, 627, 643, 735, 761, 762, 773, 789, 816, 818, 819, 885, 980, 981, 982, 983, 984, 985, 988";
    const AsignacionesVacacionesIndividual="10, 11, 12, 13, 15, 20, 21, 22, 23, 29, 30, 31, 32, 40, 41, 42, 50, 51, 60, 61, 70, 71, 72, 75, 96, 98, 100, 101, 102, 105, 106, 107, 110, 111, 112, 113, 114, 115, 118, 119, 120, 121, 123, 124, 140, 141, 151, 160, 161, 162, 170, 171, 180, 181, 190, 191, 200, 210, 220, 240, 241, 260, 261, 263, 264, 265, 268, 270, 271, 272, 273, 277, 278, 280, 288, 289, 290, 291, 300, 301, 310, 311, 320, 321, 330, 331, 340, 341, 345, 350, 351, 360, 361, 370, 371, 380, 390, 400, 410, 411, 420, 421, 430, 431, 440, 441, 450, 451, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 560, 566, 569, 571, 577, 578, 587, 588, 590, 600, 610, 611, 621, 630, 631, 640, 641, 642, 650, 761, 773, 784, 817, 824, 980, 981, 982, 983, 984, 985, 988, 990, 991, 992";
    const AsignacionesVacacionesColectivas="10, 11, 12, 15, 21, 22, 23, 29, 32, 40, 41, 42, 50, 51, 60, 61, 70, 71, 72, 102, 105, 110, 112, 114, 115, 119, 120, 121, 124, 160, 161, 170, 171, 200, 210, 220, 240, 241, 260, 261, 263, 264, 265, 270, 273, 277, 278, 280, 288, 289, 290, 291, 300, 301, 310, 311, 320, 321, 330, 331, 340, 341, 350, 351, 360, 361, 370, 371, 380, 390, 400, 410, 411, 420, 421, 430, 431, 440, 441, 450, 451, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 560, 566, 571, 578, 590, 600, 610, 630, 631, 640, 641, 642, 643, 761, 773, 885, 980, 981, 982, 983, 984, 985, 988";
    const AsignacionesPagoIntereses="240, 248, 249, 250, 520, 980, 981, 982, 983, 984, 985, 988"
    const AsignacionesUtilidades="10, 11, 12, 15, 20, 21, 22, 23, 29, 30, 31, 32, 40, 41, 42, 50, 51, 60, 61, 70, 71, 72, 102, 110, 111, 112, 124, 140, 153, 160, 161, 170, 171, 200, 210, 220, 240, 241, 260, 261, 263, 270, 273, 288, 289, 290, 291, 300, 301, 310, 311, 320, 321, 330, 331, 340, 341, 350, 351, 360, 361, 370, 371, 380, 390, 400, 410, 411, 420, 421, 430, 431, 440, 441, 450, 451, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 560, 567, 569, 570, 571, 575, 576, 587, 588, 590, 600, 610, 618, 621, 630, 631, 773, 945, 980, 981, 982, 983, 984, 985, 988";
    const AsignacionesPrestaciones="8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 21, 22, 23, 29, 30, 31, 32, 40, 41, 42, 43, 50, 51, 60, 61, 62, 70, 71, 72, 75, 76, 80, 81, 82, 90, 91, 92, 96, 97, 98, 99, 100, 101, 102, 105, 106, 107, 110, 111, 112, 113, 114, 115, 118, 119, 120, 121, 123, 124, 125, 140, 141, 150, 151, 152, 153, 154, 155, 156, 157, 159, 160, 161, 162, 170, 171, 180, 181, 190, 191, 200, 210, 220, 230, 231, 232, 240, 241, 242, 243, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 266, 267, 268, 269, 270, 271, 272, 273, 274, 277, 280, 281, 282, 288, 289, 290, 291, 300, 301, 302, 310, 311, 312, 320, 321, 330, 331, 332, 335, 336, 340, 341, 342, 350, 351, 360, 361, 370, 371, 380, 390, 400, 410, 411, 412, 420, 421, 422, 423, 430, 431, 440, 441, 450, 451, 452, 453, 460, 461, 470, 471, 480, 481, 490, 491, 500, 501, 510, 511, 520, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 560, 561, 562, 564, 565, 566, 567, 568, 569, 571, 572, 573, 574, 577, 578, 579, 583, 586, 587, 588, 589, 590, 591, 592, 593, 598, 599, 600, 601, 610, 611, 612, 613, 614, 616, 617, 618, 619, 620, 621, 622, 627, 630, 631, 640, 643, 644, 645, 646, 647, 651, 723, 761, 762, 773, 777, 784, 857, 937, 980, 981, 982, 983, 984, 985, 988";
    const CartaBancoSueldoBasicoDescuentos="010, 011, 017, 030, 031, 040, 041, 042, 060, 061, 070, 071, 072, 075, 076, 080, 081, 082, 090, 091, 092, 096, 097, 098, 099, 100, 101, 110, 111, 112, 113, 114, 118, 120, 121, 123, 140, 240, 241, 330, 331, 340, 341, 390, 390, 440, 441, 450, 451, 500, 501, 587, 588, 700, 703, 760, 770, 782, 790, 791, 792, 794, 801, 802, 803, 804, 805, 806, 808";
    const CartaBancoHorasExtraSobretiempo="043, 160, 161, 170, 171, 180, 181, 190, 191, 288, 289, 312";
    const CartaBancoBonificacion="001, 002, 003, 004, 005, 006, 007, 230, 280";
    const Concepto023Laborales="010, 011, 110, 111, 120, 121, 140, 141, 017, 030, 031, 040, 041, 042, 060, 061, 070, 071, 072, 075, 076, 080, 081, 082, 090, 091, 092, 096, 097, 098, 099, 100, 101, 112, 113, 114, 118, 123, 240, 241, 330, 331, 340, 341, 390, 440, 441, 450, 451, 500, 501, 587, 588, 043, 160, 161, 170, 171, 180, 181, 190, 191, 288, 289, 312, 001, 002, 003, 004, 005, 006, 007, 230, 280";
    const Concepto023SalarioBasico="010, 011, 110, 111, 120, 121, 140, 141";
    const Concepto023SinSalarioBasico="017, 030, 031, 040, 041, 042, 060, 061, 070, 071, 072, 075, 076, 080, 081, 082, 090, 091, 092, 096, 097, 098, 099, 100, 101, 112, 113, 114, 118, 123, 240, 241, 330, 331, 340, 341, 390, 440, 441, 450, 451, 500, 501, 587, 588, 043, 160, 161, 170, 171, 180, 181, 190, 191, 288, 289, 312, 001, 002, 003, 004, 005, 006, 007, 230, 280";
    const Concepto023Vacacionales="263, 260, 270, 277, 265, 105, 124, 120, 261, 107, 143, 121, 262, 264, 266, 267, 268, 269, 271, 272, 273, 274, 275, 278, 280, 281, 282, 583";
    
    switch ($(this).val()){
        case '1': 
            $("#conceptos").val(DeduccionesParaFiscales);
            break;
        case '2': 
            $("#conceptos").val(DeduccionesNoParaFiscales);
            break;
        case '3':  
            $("#conceptos").val(DeduccionesTodas);
            break;
        case '4': 
            $("#conceptos").val(AsignacionesTodas);
            break;
        case '5': 
            $("#conceptos").val(AsignacionesNomina);
            break;
        case '6': 
            $("#conceptos").val(AsignacionesAnticipo);
            break;
        case '7': 
            $("#conceptos").val(AsignacionesNominaEspecial);
            break;
        case '8': 
            $("#conceptos").val(AsignacionesVacacionesIndividual);
            break;
        case '9': 
            $("#conceptos").val(AsignacionesVacacionesColectivas);
            break;
        case '10': 
            $("#conceptos").val(AsignacionesPagoIntereses);
            break;
        case '11': 
            $("#conceptos").val(AsignacionesUtilidades);
            break;
        case '12':
             $("#conceptos").val(AsignacionesPrestaciones);
            break;
        case '13':
             $("#conceptos").val(CartaBancoSueldoBasicoDescuentos);
            break;
        case '14':
             $("#conceptos").val(CartaBancoHorasExtraSobretiempo);
            break;
        case '15':
             $("#conceptos").val(CartaBancoBonificacion);
            break;
        case '16':
             $("#conceptos").val(Concepto023Laborales);
            break;
        case '17':
             $("#conceptos").val(Concepto023SalarioBasico);
            break;
        case '18':
             $("#conceptos").val(Concepto023SinSalarioBasico);
            break;
        case '19':
             $("#conceptos").val(Concepto023Vacacionales);
            break;
        default:
            $("#conceptos").val("");
    }
    
}
function copiarAlPortapapeles(id_elemento){
    // Crea un campo de texto "oculto"
    var aux = document.createElement("input");

    // Asigna el contenido del elemento especificado al valor del campo
    aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);

    // Añade el campo a la página
    document.body.appendChild(aux);

    // Selecciona el contenido del campo
    aux.select();

    // Copia el texto seleccionado
    document.execCommand("copy");

    // Elimina el campo de la página
    document.body.removeChild(aux);
}
function copiarDatosTabla(idTabla){
    var strDatos="";
    var filas=document.querySelectorAll("#"+idTabla+" tbody tr");
    for(var i=0; i<filas.length;i++){
        var columnas=filas[i].querySelectorAll("td");
        if(i>0){
            strDatos+="\r\n";
        }
        for(var j=0;j<columnas.length;j++){
            if(j===0){
                strDatos+=columnas[j].innerText.toString();
            }else{
                strDatos+="\t"+columnas[j].innerText.toString();
            }
        }
    }    
    var aux = document.createElement("textarea");
    aux.innerHTML=strDatos;    
    document.body.appendChild(aux);
    aux.select();    
    document.execCommand("copy");
    document.body.removeChild(aux);
}
function filtro_keyup(event){
     //asigna el evento de soltar una tecla en el textbox filtro
    if($(this).val()===""){
        
        if(event.key!=="Enter" && event.key!=="Escape" && event.key!=="Backspace" && event.key!=="Delete"){
            $("#tblConsulta tbody").removeHighlight();
            return;
        }
        //REPAGINAR SI QUEDA VACIO EL FILTRO
        $('#myPager').html("");
        $('#cuerpoTabla').pageMe(
                                    {
                                        pagerSelector:'#myPager',
                                        showPrevNext:true,
                                        showFirstLast:true,
                                        hidePageNumbers:true,
                                        perPage:14                              
                                    }       
                                );
        activarToolTips();
    } 
    var valor=$(this).val().toLowerCase();                
    $("#tblConsulta tbody tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(valor) > -1);        
    });   
    //resaltar usando plugin jquery jquery.highlight-5.js
    $("#tblConsulta tbody").removeHighlight();
    $("#tblConsulta tbody").highlight(valor);
    if(valor!==""){
        //REPAGINAR DESPUES DEL FILTRO
        $('#myPager').html("");
        $('#cuerpoTabla').pageMe(
                                    {
                                        pagerSelector:'#myPager',
                                        showPrevNext:true,
                                        showFirstLast:true,
                                        hidePageNumbers:true,
                                        perPage:14,
                                        filter:valor                                  
                                    }       
                                );
        activarToolTips();
    }
}

/*>>>INTERACCION<<<*/
function activarToolTips(container=null){
    eliminarToolTipsViejos();
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(
            function (tooltipTriggerEl) {
                if(container===null){
                    return new bootstrap.Tooltip(tooltipTriggerEl);
                }else{
                    return new bootstrap.Tooltip(tooltipTriggerEl,{container:container});
                }
            });
}
function eliminarToolTipsViejos(){
    $(".tooltip").remove();
}
function mostrarSpinner(mensajeHTML="<b>Espere por favor</b>"){
    $("#modal-spinner-mensaje").html(mensajeHTML);
    $('#modal-spinner').css('display','flex'); 
}
function ocultarSpinner(){
    $('#modal-spinner').css('display','none'); 
}
function bloqueo_keydown(event){
    event.preventDefault();
}
function bloquearTeclado(){
    $("*").keydown(bloqueo_keydown);
}
function desbloquearTeclado(){
    $("*").unbind("keydown",bloqueo_keydown);
}
function vibrar(elementId=null, callback=null){
    var elemento=document.getElementById(elementId);
    if(elemento===null){
        return;
    }
    elemento.style.position="relative";
    var movimiento=setInterval(mover,50);
    var ciclos=8;
    var pixeles=5;
    var cuentaCiclos=0;
    var leftInicial=elemento.style.left;
    var intLeft=parseInt(leftInicial.replace("px",""));
    if(leftInicial===''){
        leftInicial='0px';
        intLeft=0;
    }
    //alert("leftInicial: '"+leftInicial+"'   intLeft:"+intLeft);
    function mover(){
        if(cuentaCiclos===ciclos){
            clearInterval(movimiento);
            elemento.style.left=leftInicial;
            if(callback!==null){
                callback();
            }
        }else{
            elemento.style.left=(intLeft+pixeles)+"px";
            pixeles*=-1;
            cuentaCiclos++;
            //alert(elemento.style.left);
        }
        
    }
}
function appendMsgbox(titulo, icono, mensaje,botones){
    $("body").append('<div class="modal" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"></div>')
    $('<div class="modal-dialog modal-dialog-centered"></div>').appendTo('#staticBackdrop');
    $('<div class="modal-content bg-dark bg-gradient rounded-10 border border-secondary  shadow"></div>').appendTo('.modal-dialog');
    $('<div class="modal-header bg-dark bg-gradient text-orange rounded-10 border-0" ></div>').appendTo('.modal-content');
    /*aqui va el titulo*/ 
    $('<h5 class="modal-title" id="staticBackdropLabel">'+titulo+'</h5>').appendTo('.modal-header');
    $('<button type="button" class="btn btn-dark text-orange border border-secondary  d-flex justify-content-center align-items-center" onclick="msgboxDialogResult = \'CANCELAR\'; modal.hide();"><i class="fa fa-times fa-fw"></i></button>').appendTo('.modal-header');
    $('<div class="modal-body bg-dark bg-gradient text-white" ></div>').appendTo('.modal-content');
    $('<div class="row"></div>').appendTo('.modal-body');
    switch(icono.toUpperCase()){
        case 'ERROR':
            icono='fad fa-times-circle fa-3x'; break;
        case 'PREGUNTA':
            icono='fad fa-question-circle fa-3x'; break;
        case 'ADVERTENCIA':
            icono='fad fa-exclamation-circle fa-3x'; break;
        case 'INFORMACION':
            icono='fad fa-info-circle fa-3x'; break;
        case 'REALIZADO':
            icono='fad fa-check-circle fa-3x'; break;
        case 'PROHIBIDO':
            icono='fad fa-lock-alt fa-3x'; break;
        default:
            icono='fad fa-info-circle fa-3x'; break;
    }
    /*aqui va el icono en la clase*/
    $('<div class="col-auto m-1"><i class="'+icono+'"></i></div>').appendTo(".modal-body .row");
    /*aqui va el mensaje*/
    $('<div class="col m-1 align-items-center d-flex"><div class="p-0 m-0">'+mensaje+'</div></div>').appendTo(".modal-body .row");
    $('<div class="modal-footer justify-content-center bg-dark bg-gradient rounded-10 border-0"></div>').appendTo('.modal-content');
    var botonesArray=botones.split(',');
    for(var i=0;i<botonesArray.length; i++){
        switch (botonesArray[i]) {
            case 'ACEPTAR':
                $('<button autofocus type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'ACEPTAR\'; modal.hide();" id="msgbox_btn_ACEPTAR" ><i class="fad fa-check me-2"></i>Aceptar</button>').appendTo('.modal-footer');
                break;
            case 'CANCELAR':
                $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'CANCELAR\';  modal.hide();" id="msgbox_btn_CANCELAR"><i class="fad fa-times me-2"></i>Cancelar</button>').appendTo('.modal-footer');
                break;
            case 'SI':
                $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'SI\';  modal.hide();" id="msgbox_btn_SI"><i class="fad fa-check me-2"></i>Sí</button>').appendTo('.modal-footer');
                break;
            case 'NO':
                $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'NO\';  modal.hide();" id="msgbox_btn_NO"><i class="fad fa-times me-2"></i>No</button>').appendTo('.modal-footer');
                break;
            default:
                $('<button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = \'ACEPTAR\';  modal.hide();" id="msgbox_btn_ACEPTAR"><i class="fad fa-check me-2"></i>Aceptar</button>').appendTo('.modal-footer');
        }
    }
    
}
async function msgbox(mensaje,titulo='Información',icono='INFORMACION',botones='ACEPTAR',enfocado='ACEPTAR',callback=null){
    let controlActivo=document.activeElement;
    let myPromise = new Promise(function (myResolve, myReject) {
           
            msgboxDialogResult="ESPERANDO";
            
            $(".modal-backdrop").remove();//ocultar fondo translucido en caso de que se llamen 2 msgbox seguidos
            $("#staticBackdrop").remove();

            //$('body').append(texto);
            appendMsgbox(titulo,icono,mensaje,botones);

            modal=new bootstrap.Modal($("#staticBackdrop"),{undefined,keyboard:true,focus:true});
            modal.show();                            
            //modal.toggle();
            $(".modal-dialog").hide();
            $(".modal-dialog").fadeIn("fast",function(){                
                                $("#msgbox_btn_"+enfocado).focus();                    
                        });
            var timer=setInterval(checkearResultado,1);
            function checkearResultado(){
                if(msgboxDialogResult!=='ESPERANDO'){
                    clearInterval(timer);
                    myResolve(msgboxDialogResult);
                }
            }    
            //ENFOCAR EL BOTON PREDETERMINADO
            setTimeout(function(){
                $("#msgbox_btn_"+enfocado).focus();  

            },100); 
            
    });    
    var resultado=await myPromise;
    $("#staticBackdrop").remove();//quitar el dialogo oculto que queda luego de cerrarlo
    $("body").removeAttr("class").removeAttr("style").removeAttr("data-bs-padding-right").removeAttr("data-bs-overflow");//quitar atributos que quedan luego de cerrar el dialogo
    $(controlActivo).focus();
    return resultado;
}

function exportTableToCSV(tableID,filename='') {//TOMADO DE https://programacion.net/articulo/como_exportar_una_tabla_html_a_csv_mediante_javascript_1742
    var csv = [];
    var rows = document.querySelectorAll("#"+tableID+" tr");//LINEA MODIFICADA PARA USAR tableID
    
    for (var i = 0; i < rows.length; i++) {
        var row = [];
        var cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push('"'+cols[j].innerText.toString()+'"');//LINEA MODIFICADO PARA COMPATIBILIDAD CON EXCEL
        
        csv.push(row.join(";"));//LINEA MODIFICADO PARA COMPATIBILIDAD CON EXCEL
    }

    // Download CSV file
    downloadCSV(csv.join("\r\n"), filename);
}
function exportTableToTXT(tableID,filename){
    var strDatos="";
    var filas=document.querySelectorAll("#"+tableID+" tbody tr");
    for(var i=0; i<filas.length;i++){
        var columnas=filas[i].querySelectorAll("td");
        if(i>0){
            strDatos+="\r\n";
        }
        for(var j=0;j<columnas.length;j++){
            if(j===0){
                strDatos+=columnas[j].innerText.toString();
            }else{
                strDatos+="\t"+columnas[j].innerText.toString();
            }
        }
    }
    // Download TXT file
    downloadTXT(strDatos, filename);
}
function downloadCSV(csv, filename) {//TOMADO DE https://programacion.net/articulo/como_exportar_una_tabla_html_a_csv_mediante_javascript_1742
    var csvFile;
    var downloadLink;

    // Specify file name
    filename = filename?filename+'.csv':'csv_data.csv';
    
    //definir el BOM de utf8
    var BOM='\ufeff';
    
    // CSV file
    csvFile = new Blob([BOM, csv], {type: "text/csv;"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}
function downloadTXT(txt, filename) {
    var txtFile;
    var downloadLink;

    // Specify file name
    filename = filename?filename+'.txt':'txt_data.txt';
    
    //definir el BOM de utf8
    var BOM='\ufeff';
    
    // TXT file
    txtFile = new Blob([BOM, txt], {type: "text/txt;"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(txtFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}
$(document).ready(document_ready);
