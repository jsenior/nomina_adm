<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Casb_Modelo
 *
 * @author jsenior
 */
class Rep_modelo extends CI_Model{

    private $Biblioteca;

    public function __construct(){
        parent::__construct();
        $this->db=$this->load->database('S1044B2C',true,true);
        $this->Biblioteca="SPI";
    }

    // Funcion consulta centros de costos que esten activos
    // El parametro $tiponomina es el valor de los numeros de nomina que se trae del controlador
    public function get_consulta_cc($tipnom="", $per=0){

        // Consulta original con NOT IN ..... a efectyos de mostrar datos ........
        $sql="SELECT 
        TPROCE, TIPNOM, CLASE,  PER012, USER12, WI12, FICH12, CLAV12, CAN012, BOL012, 
        FECM12, DPTO12, MAY012
      FROM 
        SPI.NMPP012 
      WHERE 
      TIPNOM IN ({$tipnom}) AND
      PER012 IN ({$per}) AND
      YEAR(NOW()) = SUBSTR(FECM12,1,4) AND 
        SUBSTR(MAY012,1,1) = '2' AND
        SUBSTR(MAY012,7,4) NOT  IN (SELECT AUXI12 
                                    FROM SFDATA.KN012 
                                    WHERE CLAU12 = 'CC' AND
                                         STAT12 = 'A') FETCH FIRST 1000 ROWS ONLY";
        return $this->db->query($sql);
    }

    public function get_tipnom(){//TIPOS DE NOMINAS QUE ESTAN ACTIVAS
    
             $SELECT="
                SELECT 
                  TRIM(KEYTAB) TIPNOM,
                  TRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REGTAB,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')) DESTIPNOM
                FROM SPI.NMPP004
                WHERE TIPREG=001 AND KEYTAB IN (SELECT DISTINCT TIPNOM FROM SPI.NMPP007 WHERE STATUS!=0)
                ORDER BY KEYTAB
                     ";
             $resultado=$this->db->query($SELECT)->result_array();
             $numeros=array();
             $letras=array();
             foreach($resultado as $fila){
                 if(is_numeric($fila['TIPNOM'])){
                     $numeros[]=$fila;
                 }else{
                     $letras[]=$fila;
                 }
             }
             return array_merge($numeros,$letras); 
         }

   public function periodo_activo($tipnom){ // PERIODOS ACTIVOS

            $SELECT="
            SELECT DISTINCT
            TAB01.NUMPER 
          FROM SPI.NMPP001 TAB01  
          WHERE     
            TAB01.TIPCOM='61' 
            AND TAB01.TIPPRO='1' 
            AND TAB01.PERPRO='0'  
            AND TAB01.TIPNOM IN ({$tipnom})
            AND TAB01.NUMPER=(SELECT MAX(NUMPER) 
                   FROM SPI.NMPP001 TAB02 
                   WHERE 
                        TAB02.TIPCOM=TAB01.TIPCOM 
                        AND TAB02.TIPPRO=TAB01.TIPPRO 
                        AND TAB02.PERPRO= TAB01.PERPRO 
                        AND TAB02.TIPNOM=TAB01.TIPNOM
                        AND TAB02.AÑOCAL= (SELECT MAX(AÑOCAL) 
                                           FROM SPI.NMPP001 TAB03 
                                           WHERE 
                                                TAB03.TIPCOM=TAB02.TIPCOM 
                                                AND TAB03.TIPPRO=TAB02.TIPPRO 
                                                AND TAB03.PERPRO= TAB02.PERPRO 
                                                AND TAB03.TIPNOM=TAB02.TIPNOM)  
                              )  
         
            ";
            // Se debe decodificar el UTF8 para que lo entienda el as400.
            $SELECT=utf8_decode($SELECT);
            $resultado=$this->db->query($SELECT)->result_array();

            return $resultado;
   }      

}