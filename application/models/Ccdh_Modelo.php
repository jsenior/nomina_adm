<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Casb_Modelo
 *
 * @author rugonzalez
 */
class Ccdh_Modelo extends CI_Model{
    //put your code here
    private $Biblioteca;
    public function __construct(){
        parent::__construct();
        $this->db=$this->load->database('S1044B2C',true,true);
        $this->Biblioteca="SPI";
    }
    public function get_periodo_abierto($tiponomina){
        $sql="
                SELECT NUMPER NUMERO, 
                SUBSTR(FECINI,1,4) ANO,
                MESCAL MES,
                SUBSTR(FECINI,1,4)||'/'||SUBSTR(FECINI,5,2)||'/'||SUBSTR(FECINI,7,2) FECHAINICIAL,
                SUBSTR(FECFIN,1,4)||'/'||SUBSTR(FECFIN,5,2)||'/'||SUBSTR(FECFIN,7,2) FECHAFINAL
                FROM SPI.NMPP001
                WHERE PERPRO=0 
                AND TIPPRO=1 
                AND TIPNOM='{$tiponomina}'                
                ORDER BY FECINI DESC
            ";
                
                try{
                    $result=$this->db->query($sql)->result_array();
                    if($result==null)
                        return array("NUMERO"=>0,"ANO"=> date("Y"),"FECHAINICIAL"=>'00/00/0000',"FECHAFINAL"=>'00/00/0000');
                    else
                        return $result[0];                        
                }catch(Exception $e){
                    return array("NUMERO"=>0,"ANO"=> date("Y"),"FECHAINICIAL"=>'00/00/0000',"FECHAFINAL"=>'00/00/0000');
                } 
    }
    
    public function get_año_periodo($tiponomina){
        $sql="
                SELECT SUBSTR(FECINI,1,4) ANO 
                FROM SPI.NMPP001
                WHERE PERPRO=0 
                AND TIPPRO=1 
                AND TIPNOM='{$tiponomina}'                
                ORDER BY FECINI DESC
            ";
        return (int)($this->db->query($sql)->result_array()[0]['NUMPER']);
    }
    
    public function get_max_field_value($field,$tabla,$filtro='1=1'){
       $max=0;
       $result=$this->db->query("SELECT MAX(".$field.") MAXVALUE FROM ".$this->Biblioteca.".".$tabla." WHERE ".$filtro)->result_array();
       if(count($result)>0){
           $max=$result[0]['MAXVALUE'];
       }else{
           $max=0;
       }
       return $max;
   }
   public function ExpandirRangoEnteroEnLista($valores){
       $valores= explode(",", $valores);
       foreach ($valores as $index=>$valor ){
           if( str_contains($valor, "-")){
                $rango=explode("-",$valor);
                $rango[0]=(int)$rango[0];
                $rango[1]=(int)$rango[1];
                $valor="";
                for($i=$rango[0];i<=$rango[1]; $i++){
                    if($i==$rango[0]){
                        $valor="".$i;
                    }else{
                        $valor.=", ".$i;
                    }
                }
                $valores[$index]=$valor;
            }
       }
       $valores= implode(", ", $valores);
       return $valores;
   }
   public function sql_exec($sql){
       return $this->db->simple_query($sql);
   }
   public function generar_sql_insert($nombreTabla,$dataArray){
        $sql_insert="";
        $sql_insert ="INSERT INTO ".$this->Biblioteca.".".$nombreTabla." ";
        //$sql_insert ="INSERT INTO HISTORY.".$nombreTabla." ";
        
        //  <PREPARAR NOMBRES DE CAMPOS>        
        $campos="";
        $index=0;
        foreach($dataArray[0] as $key => $valor){
            if($index==0){
                $campos.=$key;
            }else{
                $campos.=", ".$key;
            }
            $index++;
        }
        $campos=" (".$campos.")\r\n";
        //  </PREPARAR NOMBRES DE CAMPOS>
        
        //<PREPARAR REGISTROS>
        $registros="VALUES\r\n";
        foreach($dataArray as $i => $fila){
            if($i==0){
                $registros.="(";
            }else{
                $registros.=",\r\n(";
            }            
            $index=0;
            foreach($fila as $key=>$valor){
                if($index==0){
                    if(is_string($valor)){
                        $registros.="'".$valor."'";                    
                    }else{
                        $registros.=$valor;
                    }                    
                }else{
                    if(is_string($valor)){
                        $registros.=", "."'".$valor."'";                    
                    }else{
                        $registros.=", ".$valor;
                    }
                }
                $index++;
            }            
            $registros.=")";
        }
        
        //</PREPARAR REGISTROS>
        
        //<ARMAR LA CONSULTA>
        $sql_insert.=$campos.$registros;
        //</ARMAR LA CONSULTA>
        
        return $sql_insert;
   }
   public function get_tipnom(){//TIPOS DE NOMINAS QUE ESTAN ACTIVAS
//       $SELECT="SELECT DISTINCT TIPNOM, (SELECT TRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REGTAB,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')) DESTIPNOM "
//               . "FROM ".$this->Biblioteca.".NMPP004 "
//               . "WHERE TIPREG=001 AND KEYTAB=T01.TIPNOM) DESTIPNOM FROM SPI.NMPP007 T01 WHERE STATUS<>0 ORDER BY TIPNOM ASC";
//       
       $SELECT="
          SELECT 
            TRIM(KEYTAB) TIPNOM,
            TRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REGTAB,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','')) DESTIPNOM
          FROM SPI.NMPP004
          WHERE TIPREG=001 AND KEYTAB IN (SELECT DISTINCT TIPNOM FROM SPI.NMPP007 WHERE STATUS!=0)
          ORDER BY KEYTAB
               ";
       $resultado=$this->db->query($SELECT)->result_array();
       $numeros=array();
       $letras=array();
       foreach($resultado as $fila){
           if(is_numeric($fila['TIPNOM'])){
               $numeros[]=$fila;
           }else{
               $letras[]=$fila;
           }
       }
       return array_merge($numeros,$letras); 
   }
   public function get_listado_trabajadores(
           $ingresomin=''
           ,$ingresomax=''
           ,$tiempomin=''
           ,$tiempomax=''
           ,$tiposnominas=''
           ,$sexo='A'
           ,$grupo='A'
           ,$status=''
           ,$edadmin=''
           ,$edadmax=''
           ,$operadortalleres='igual'
           ,$talleres=''
           ,$operadorfichas='igual'
           ,$fichas=''){
        $where="";
        $ln="\r\n";
        if(!empty($fichas)){
            if($operadorfichas==='igual'){
                $where.=$ln." AND TRIM(FICHA) IN ({$fichas})";
            }else{
                $where.=$ln." AND TRIM(FICHA) NOT IN ({$fichas})";
            }
        }
        if($sexo=='M'){
            $where.=$ln." AND SEXO=1";
        }
        if($sexo=='F'){
            $where.=$ln." AND SEXO=2";
        }
        if($grupo=='HG'){//HACE GUARDIA = HG
            $where.=$ln." AND GRUPOM='G'";
        }
        if($grupo=='NHG'){//NO HACE GUARDIA = NHG
            $where.=$ln." AND GRUPOM='A'";
        }
        if(!empty($ingresomin) && !empty($ingresomax)){
            $where.=$ln." AND FECING BETWEEN {$ingresomin} AND {$ingresomax}";
        }
        if(!empty($tiposnominas)){
            $where.=$ln." AND TIPNOM IN ({$tiposnominas})";
        }  
        if(!empty($talleres)){
            if($operadortalleres==='igual'){
                $where.=$ln." AND CODDEP IN ({$talleres})";                
            }else{
                $where.=$ln." AND CODDEP NOT IN ({$talleres})";
            }
        }
        if($status=='V'){
            $where.=$ln." AND  FSALVC<=(YEAR(CURRENT DATE)||RIGHT(REPEAT('0',2)||MONTH(CURRENT DATE),2)||RIGHT(REPEAT('0',2)||DAY(CURRENT DATE),2)*1) AND (YEAR(CURRENT DATE)||RIGHT(REPEAT('0',2)||MONTH(CURRENT DATE),2)||RIGHT(REPEAT('0',2)||DAY(CURRENT DATE),2)*1)<FREGVC";
        }
        if($status=='T'){
            $where.=$ln." AND NOT (FSALVC<=(YEAR(CURRENT DATE)||RIGHT(REPEAT('0',2)||MONTH(CURRENT DATE),2)||RIGHT(REPEAT('0',2)||DAY(CURRENT DATE),2)*1) AND (YEAR(CURRENT DATE)||RIGHT(REPEAT('0',2)||MONTH(CURRENT DATE),2)||RIGHT(REPEAT('0',2)||DAY(CURRENT DATE),2)*1)<FREGVC)";
        }
        if(!empty($edadmin) && !empty($edadmax)){
            $where.=$ln." AND FLOOR((DOUBLE(DAYS(CURDATE())-DAYS(SUBSTRING(FECNAC,1,4)||'-'||SUBSTRING(FECNAC,5,2)||'-'||SUBSTRING(FECNAC,7,2)))-FLOOR((DOUBLE(CHAR(YEAR(CURDATE())))-SUBSTRING(FECNAC,1,4))/4))/365) BETWEEN {$edadmin} AND {$edadmax}";
        }
        
        if(!empty($tiempomin) && !empty($tiempomax)){
            $where.=$ln." AND FLOOR((DOUBLE(DAYS(CURDATE())-DAYS(SUBSTRING(FECING,1,4)||'-'||SUBSTRING(FECING,5,2)||'-'||SUBSTRING(FECING,7,2)))-FLOOR((DOUBLE(CHAR(YEAR(CURDATE())))-SUBSTRING(FECING,1,4))/4))/365) BETWEEN {$tiempomin} AND {$tiempomax}";
        }
        
        
        $sql="
            SELECT                 
                TIPNOM
                ,CODDEP
                ,TRIM(FICHA) FICHA                
                ,CEDULA
                ,NOMBRE
                ,DATE(SUBSTRING(FECING,1,4)||'-'||SUBSTRING(FECING,5,2)||'-'||SUBSTRING(FECING,7,2)) FECING
                ,FLOOR((DOUBLE(DAYS(CURDATE())-DAYS(SUBSTRING(FECING,1,4)||'-'||SUBSTRING(FECING,5,2)||'-'||SUBSTRING(FECING,7,2)))-FLOOR((DOUBLE(CHAR(YEAR(CURDATE())))-SUBSTRING(FECING,1,4))/4))/365) TIEMPO
                ,GRUPOM
                ,(CASE SEXO WHEN 1 THEN 'M' WHEN 2 THEN 'F' ELSE 'M' END) SEXO
                ,FLOOR((DOUBLE(DAYS(CURDATE())-DAYS(SUBSTRING(FECNAC,1,4)||'-'||SUBSTRING(FECNAC,5,2)||'-'||SUBSTRING(FECNAC,7,2)))-FLOOR((DOUBLE(CHAR(YEAR(CURDATE())))-SUBSTRING(FECNAC,1,4))/4))/365) EDAD
                ,SUBSTRING(FSALVC,1,4)||'-'||SUBSTRING(FSALVC,5,2)||'-'||SUBSTRING(FSALVC,7,2) FSALVC
                ,SUBSTRING(FREGVC,1,4)||'-'||SUBSTRING(FREGVC,5,2)||'-'||SUBSTRING(FREGVC,7,2) FREGVC
                ,SUEMEN  
                ,SUELDO
                ,0 PERIODO
                ,'' CONCEPTO
                ,0.0 CANTIDAD
                ,0.0 MONTO
                ,'' CUENTA
                ,'' EXCLUIR
            FROM SPI.NMPP007 
            WHERE
                STATUS!=0
                {$where}           
            ";
        //return array("sql"=>$sql);        
        return $this->db->query($sql);
   }
   public function get_consulta_principal($año="",$mes="",$periodo="",$procesos="",$conceptos="",$tipnom=""){
           
       
       if(empty($año) || empty($mes)||empty($conceptos)||empty($tipnom)||empty($periodo)){
           return null;    
       }       
       //SUM(CASE WHEN CLAVE IN(700, 760, 770, 782, 790) THEN  -1*T01.BOL014 ELSE T01.BOL014 END) MONTO
       $sql="
            SELECT 
               SUM(CASE WHEN CLAVE>=700 THEN  -1*T01.BOL014 ELSE T01.BOL014 END) MONTO
              ,(CASE MAX(T02.NACION) WHEN 1 THEN 'V' WHEN 2 THEN 'E' ELSE 'V' END) || (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(MAX(T02.CEDULA),'E',''),'V',''),'.',''),'-',''),' ','')) CEDULA
              ,TRIM(MAX(T02.NOMSEN)) NOMBRE
              ,TRIM(MAX(T02.CTABAN)) CUENTA
            FROM SPI.NMPP007 T02 INNER JOIN SPI.NMPP034 T01 ON T01.FICHA=T02.FICHA
            WHERE 1=1 --FIJO
              AND T01.TIPPRO IN ({$procesos})
              AND T01.CIA='01'--fijo
              AND T01.TIPNOM IN ({$tipnom})
              AND T01.AÑOCAL={$año}
              AND T01.MESCAL={$mes}              
              AND T01.NUMPER={$periodo}               
              AND T01.ANULA=0 --FIJO
              AND T01.CLAVE IN ({$conceptos})
              AND T01.BOL014!=0.00 --fijo para impedir que aparezcan montos en 0
            GROUP BY 
              T01.FICHA
            ORDER BY 
              T01.FICHA               
            ";  
       $sql=utf8_decode($sql);
       return $this->db->query($sql);
   }
   
   public function get_porficha($año="",$desdemes="",$hastames="",$procesos="",$conceptos="",$tipnom="",$poronapre="",$pordianca="",$fichas="",$talleres=""){
         
       
       if(empty($año) || empty($desdemes) || empty($hastames) || empty($conceptos) || empty($procesos) || empty($tipnom) || (empty($poronapre) && empty($pordianca))){
           return null;    
       }             
       
       $where_fichas="";       
       if(!empty($fichas)){
           $where_fichas="AND N34.FICHA IN ({$fichas}) ";
       }
       
       $where_talleres="";       
       if(!empty($talleres)){
           $where_talleres="AND N34.DPTO14 IN ({$talleres}) ";
       }
       
       $sql="
              SELECT   
                N34.AÑOCAL AÑO
                ,N34.MESCAL MES
                ,N34.TIPPRO PROCESO
                ,N34.NUMPER PERIODO
                ,MAX(N34.TIPNOM) NOMINA
                ,N34.FICHA FICHA
                ,MAX(N34.DPTO14) TALLER
                ,(CASE MAX(N7.NACION) WHEN 1 THEN 'V' WHEN 2 THEN 'E' ELSE 'V' END) || (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(MAX(N7.CEDULA),'E',''),'V',''),'.',''),'-',''),' ','')) CEDULA
                ,MAX(TRIM(N7.NOMBRE)) NOMBRE
                ,N34.CLAVE CONCEPTO
                ,MAX(TRIM(N6.DESCT1)) DESCRIPCION
                ,SUM(N34.CAN014) CANTIDAD
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=0 THEN SUM(N34.BOL014) ELSE 0 END) ASIGNACION_TRABAJADOR
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.BOL014) ELSE 0 END) DEDUCCION_TRABAJADOR                
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14) ELSE SUM(N34.BOL014) END) MONTO_COMPAÑIA
                /*,SUM(N34.APOR14*{$poronapre}/100) MONTO_ONAPRE*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$poronapre}/100) ELSE SUM(N34.BOL014*{$poronapre}/100) END) MONTO_ONAPRE  
                /*,SUM(N34.APOR14*{$pordianca}/100) MONTO_DIANCA*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$pordianca}/100) ELSE SUM(N34.BOL014*{$pordianca}/100) END) MONTO_DIANCA  
              FROM 
                SPI.NMPP034 N34
                INNER JOIN (SELECT FICHA,NOMBRE,CEDULA,NACION FROM SPI.NMPP007) N7 ON (N34.FICHA=N7.FICHA)
                INNER JOIN (SELECT TIPNOM,CLAVE,DESCT1 FROM SPI.NMPP006) N6 ON (N34.TIPNOM=N6.TIPNOM AND N34.CLAVE=N6.CLAVE)
              WHERE ANULA=0
                AND N34.AÑOCAL={$año} 
                AND N34.TIPPRO in ({$procesos})
                AND N34.MESCAL BETWEEN {$desdemes} AND {$hastames} 
                AND N34.TIPNOM IN ({$tipnom})
                AND N34.CLAVE IN ({$conceptos})
                {$where_fichas}
                {$where_talleres}
              GROUP BY 
                N34.AÑOCAL,N34.MESCAL,N34.TIPPRO,N34.NUMPER,N34.FICHA,N34.CLAVE
              ORDER BY
                N34.FICHA,N34.AÑOCAL,N34.MESCAL,N34.TIPPRO,N34.NUMPER,N34.CLAVE               
            ";  
       $sql=utf8_decode($sql);
       return $this->db->query($sql);
   }
   
   public function get_porperiodo($año="",$desdemes="",$hastames="",$procesos="",$conceptos="",$tipnom="",$poronapre="",$pordianca="",$fichas="",$talleres=""){
           
       
       if(empty($año) || empty($desdemes) || empty($hastames) || empty($conceptos) || empty($procesos) || empty($tipnom) || (empty($poronapre) && empty($pordianca))){
           return null;    
       }        
       
       $where_fichas="";       
       if(!empty($fichas)){
           $where_fichas="AND N34.FICHA IN ({$fichas}) ";
       }
       
       $where_talleres="";       
       if(!empty($talleres)){
           $where_talleres="AND N34.DPTO14 IN ({$talleres}) ";
       }
       
       $sql="
              SELECT   
                N34.AÑOCAL AÑO
                ,N34.MESCAL MES
                ,N34.NUMPER PERIODO
                ,MAX(N34.TIPNOM) NOMINA
                ,N34.CLAVE CONCEPTO
                ,MAX(TRIM(N6.DESCT1)) DESCRIPCION
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=0 THEN SUM(N34.BOL014) ELSE 0 END) ASIGNACION_TRABAJADOR
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.BOL014) ELSE 0 END) DEDUCCION_TRABAJADOR                
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14) ELSE SUM(N34.BOL014) END) MONTO_COMPAÑIA
                /*,SUM(N34.APOR14*{$poronapre}/100) MONTO_ONAPRE*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$poronapre}/100) ELSE SUM(N34.BOL014*{$poronapre}/100) END) MONTO_ONAPRE  
                /*,SUM(N34.APOR14*{$pordianca}/100) MONTO_DIANCA*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$pordianca}/100) ELSE SUM(N34.BOL014*{$pordianca}/100) END) MONTO_DIANCA  
              FROM 
                SPI.NMPP034 N34
                INNER JOIN (SELECT TIPNOM,CLAVE,DESCT1 FROM SPI.NMPP006) N6 ON (N34.TIPNOM=N6.TIPNOM AND N34.CLAVE=N6.CLAVE)
              WHERE ANULA=0
                AND N34.AÑOCAL={$año} 
                AND N34.TIPPRO in ({$procesos})
                AND N34.MESCAL BETWEEN {$desdemes} AND {$hastames} 
                AND N34.TIPNOM IN ({$tipnom})
                AND N34.CLAVE IN ({$conceptos})
                {$where_fichas}   
                {$where_talleres}
              GROUP BY 
                N34.AÑOCAL,N34.MESCAL,N34.NUMPER,N34.CLAVE
              ORDER BY
                N34.CLAVE,N34.AÑOCAL,N34.MESCAL,N34.NUMPER               
            ";  
       $sql=utf8_decode($sql);
       return $this->db->query($sql);
   }
   public function get_pormes($año="",$desdemes="",$hastames="",$procesos="",$conceptos="",$tipnom="",$poronapre="",$pordianca="",$fichas="",$talleres=""){
           
       
       if(empty($año) || empty($desdemes) || empty($hastames) || empty($conceptos) || empty($procesos) || empty($tipnom) || (empty($poronapre) && empty($pordianca))){
           return null;    
       }      
       
       $where_fichas="";       
       if(!empty($fichas)){
           $where_fichas="AND N34.FICHA IN ({$fichas}) ";
       }
       
       $where_talleres="";       
       if(!empty($talleres)){
           $where_talleres="AND N34.DPTO14 IN ({$talleres}) ";
       }
       
       $sql="
              SELECT   
                N34.AÑOCAL AÑO
                ,N34.MESCAL MES
                ,N34.CLAVE CONCEPTO
                ,MAX(TRIM(N6.DESCT1)) DESCRIPCION
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=0 THEN SUM(N34.BOL014) ELSE 0 END) ASIGNACION_TRABAJADOR
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.BOL014) ELSE 0 END) DEDUCCION_TRABAJADOR                
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14) ELSE SUM(N34.BOL014) END) MONTO_COMPAÑIA
                /*,SUM(N34.APOR14*{$poronapre}/100) MONTO_ONAPRE*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$poronapre}/100) ELSE SUM(N34.BOL014*{$poronapre}/100) END) MONTO_ONAPRE  
                /*,SUM(N34.APOR14*{$pordianca}/100) MONTO_DIANCA*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$pordianca}/100) ELSE SUM(N34.BOL014*{$pordianca}/100) END) MONTO_DIANCA  
              FROM 
                SPI.NMPP034 N34
                INNER JOIN (SELECT TIPNOM,CLAVE,DESCT1 FROM SPI.NMPP006) N6 ON (N34.TIPNOM=N6.TIPNOM AND N34.CLAVE=N6.CLAVE)
              WHERE ANULA=0
                AND N34.AÑOCAL={$año} 
                AND N34.TIPPRO in ({$procesos})
                AND N34.MESCAL BETWEEN {$desdemes} AND {$hastames} 
                AND N34.TIPNOM IN ({$tipnom})
                AND N34.CLAVE IN ({$conceptos})
                {$where_fichas}
                {$where_talleres}
              GROUP BY 
                N34.AÑOCAL,N34.MESCAL,N34.CLAVE
              ORDER BY
                N34.CLAVE,N34.AÑOCAL,N34.MESCAL              
            ";  
       $sql=utf8_decode($sql);
       return $this->db->query($sql);
   }
   public function get_porconcepto($año="",$desdemes="",$hastames="",$procesos="",$conceptos="",$tipnom="",$poronapre="",$pordianca="", $fichas="",$talleres=""){
           
       
       if(empty($año) || empty($desdemes) || empty($hastames) || empty($conceptos) || empty($procesos) || empty($tipnom) || (empty($poronapre) && empty($pordianca))){
           return null;    
       }       
       
       $where_fichas="";       
       if(!empty($fichas)){
           $where_fichas="AND N34.FICHA IN ({$fichas}) ";
       }
       
       $where_talleres="";       
       if(!empty($talleres)){
           $where_talleres="AND N34.DPTO14 IN ({$talleres}) ";
       }
       
       $sql="
              SELECT   
                N34.AÑOCAL AÑO
                ,N34.CLAVE CONCEPTO
                ,MAX(TRIM(N6.DESCT1)) DESCRIPCION
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=0 THEN SUM(N34.BOL014) ELSE 0 END) ASIGNACION_TRABAJADOR
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.BOL014) ELSE 0 END) DEDUCCION_TRABAJADOR                
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14) ELSE SUM(N34.BOL014) END) MONTO_COMPAÑIA
                /*,SUM(N34.APOR14*{$poronapre}/100) MONTO_ONAPRE*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$poronapre}/100) ELSE SUM(N34.BOL014*{$poronapre}/100) END) MONTO_ONAPRE  
                /*,SUM(N34.APOR14*{$pordianca}/100) MONTO_DIANCA*/
                ,(CASE WHEN (SELECT DEDCTO FROM SPI.NMPP006 WHERE TIPNOM=MAX(N34.TIPNOM) AND CLAVE=N34.CLAVE GROUP BY CLAVE,DEDCTO)=1 THEN SUM(N34.APOR14*{$pordianca}/100) ELSE SUM(N34.BOL014*{$pordianca}/100) END) MONTO_DIANCA  
              FROM 
                SPI.NMPP034 N34
                INNER JOIN (SELECT TIPNOM,CLAVE,DESCT1 FROM SPI.NMPP006) N6 ON (N34.TIPNOM=N6.TIPNOM AND N34.CLAVE=N6.CLAVE)
              WHERE ANULA=0
                AND N34.AÑOCAL={$año} 
                AND N34.TIPPRO in ({$procesos})
                AND N34.MESCAL BETWEEN {$desdemes} AND {$hastames} 
                AND N34.TIPNOM IN ({$tipnom})
                AND N34.CLAVE IN ({$conceptos})
                {$where_fichas}
                {$where_talleres}
              GROUP BY 
                N34.AÑOCAL,N34.CLAVE
              ORDER BY
                N34.CLAVE,N34.AÑOCAL             
            ";  
       $sql=utf8_decode($sql);
       return $this->db->query($sql);
   }
}
