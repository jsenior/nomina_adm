<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_controller extends CI_Controller {
    
    public $ruta;
    public $javascript;

    public function __construct() {
        
        parent::__construct();
        $this->ruta="/repnc/";
        $this->load->model("Rep_modelo");
        $this->load->library('table');
        $this->javascript='<script src="'.$this->ruta.'js/nom.adm.js" class="code-hide"></script>';
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('table');
    }

    Public function index () {

        $datos['tipnom']=$this->Rep_modelo->get_tipnom();
        $datos['javascript']=$this->javascript;       
        $this->load->view('plantillas/encabezado_nom_adm',$datos);
        $this->load->view('rep_vista',$datos);
        $this->load->view('plantillas/pie_nom_adm');


    }

    Public function consulta_nom() {

        $tiposnominas="";        
        foreach($_POST AS $key => $valor){
            if($valor=="on"){//SI ES UN CHECKBOX Y ESTA MARCADO
                if($tiposnominas==""){
                    $tiposnominas="'".str_replace("tiponomina_", "", $key)."'";
                }else{
                    $tiposnominas.=",'".str_replace("tiponomina_", "", $key)."'";
                }
            }  
        }

       
        $periodo=$this->Rep_modelo->periodo_activo($tiposnominas); 
       
         
         
       $peri='';      
       foreach($periodo AS $i => $fila){
          foreach($fila AS $key => $valor){
            if($i==0){
                $peri=$peri.$valor;                
            }else{
                $peri=$peri.','.$valor;
            } 
          }
          }  
      
          print_r($peri);


        $result=$this->Rep_modelo->get_consulta_cc($tiposnominas, $peri); 
       //print_r($tiposnominas); 
        $datos['reporte']=$result;
        $this->load->view('revisar_tabla',$datos);

    }
    
}