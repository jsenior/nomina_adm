<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccdh extends CI_Controller {

    public $titulo;
    public $ruta;
    public $modulo;
    public $javascript;
    public function __construct() {
        parent::__construct();
        set_time_limit(60*30);//reinicia el contador en 0 y espera un maximo de 5 minutos  
        ini_set("memory_limit","1024M"); //1GB de RAM para PHP
        $this->titulo="Consulta Conceptos Deducciones Histórico";
        $this->ruta="/CI3CCDH/";
        $this->modulo="CCDH";
        $this->javascript='<script src="'.$this->ruta.'js/ccdh.js" class="code-hide"></script>';
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('table');
        $this->load->model('ccdh_modelo');
    }
    
    public function index(){
        if(!$this->PuedeEntrar()){
            return;
        }  
        /*DESCOMENTAR MIENTRAS SE ACTUALIZA EL MODULO EN CALIENTE>>>*/
        //      $this->EnConstruccion(); return;
        /*<<<DESCOMENTAR MIENTRAS SE ACTUALIZA EL MODULO EN CALIENTE*/
        $variables=array(
            'titulo'=>$this->titulo,
            'ruta'=>$this->ruta,
            'modulo'=>$this->modulo,
            'javascript'=>$this->javascript,
            'nombre_usuario'=>$this->session->usuario['nom'].' '. $this->session->usuario['ape']
        );
        $result=$this->ccdh_modelo->get_periodo_abierto('3');        
        $variables['periodo_actual']=$result['NUMERO'];
        $variables['año_actual']=$result['ANO'];
        $variables['tipnom']=$this->ccdh_modelo->get_tipnom();
        $this->load->view('plantillas/encabezado',$variables);
        $this->load->view('ccdh',$variables);
        $this->load->view('plantillas/pie',$variables);
    }
    
    public function PuedeEntrar(){
        //OBTENGO EL VALOR DEL ARRAY ESTA_AUTENTICADO DE LA $_SESSION 
        $esta_autenticado=$this->session->esta_autenticado;
        //
        if(!$esta_autenticado[$this->ruta]){
            $this->session->modulo_requerido=$this->ruta;
            $esta_autenticado[$this->ruta]=false;
            $this->session->esta_autenticado=$esta_autenticado;
            redirect('..'.'/CI3CAMD/');
            return false;
        }
        return true;
    }
    public function PuedeSeguir(){
        //OBTENGO EL VALOR DEL ARRAY ESTA_AUTENTICADO DE LA $_SESSION 
        $esta_autenticado=$this->session->esta_autenticado;
        //
        if(!$esta_autenticado[$this->ruta]){
            $this->session->modulo_requerido=$this->ruta;
            $esta_autenticado[$this->ruta]=false;
            $this->session->esta_autenticado=$esta_autenticado;
            echo "<script>window.location.assign('{$this->ruta}');</script>";
            return false;
        }
        return true;
    }
    public function EnConstruccion(){
        $datos['titulo']=$this->titulo;
        $datos['modulo']=$this->modulo;
        $datos['ruta']=$this->ruta;        
        $this->load
                ->view('plantillas/encabezado',$datos)
                ->view('plantillas/enconstruccion')
                ->view('plantillas/pie');
    }
    public function previsualizar(){
        if(!$this->PuedeSeguir()){//evita que se ejecute la accion si no esta autenticado el usuario
            return;
        } 
        
        $tiposnominas="";        
        foreach($_POST AS $key => $valor){
            if($valor=="on"){//SI ES UN CHECKBOX Y ESTA MARCADO
                if($tiposnominas==""){
                    $tiposnominas="'".str_replace("tiponomina_", "", $key)."'";
                }else{
                    $tiposnominas.=",'".str_replace("tiponomina_", "", $key)."'";
                }
            }  
        }
        $tituloconsulta="";
        switch($this->input->post('grupovistas')){
            case '4':
                $tituloconsulta="Detallada por Ficha"; break;
            case '3':
                $tituloconsulta="Resumida por Periodo"; break;
            case '2':
                $tituloconsulta="Resumida por Mes"; break;
            case '1':
                $tituloconsulta="Resumida por Concepto"; break;
            default:
                $tituloconsulta="No establecido";
        }
        $tiposnominas=str_replace(",'todostipnom'","",$tiposnominas);//elimina el check todostipnom
        $tp=$tiposnominas;
        $tp= str_replace("'","",$tp);
        $tituloconsulta.=" N({$tp})";
    
        $conceptos=$this->input->post('conceptos');
        $conceptos= $this->ExpandirConceptos($conceptos);
        
        $fichas=$this->input->post('fichas');        
        $fichas=$this->preparar_fichas($fichas);
        
        $talleres=$this->input->post('talleres');
        
        $año=$this->input->post('ano');        
        $desdemes=$this->input->post('desdemes');   
        $hastames=$this->input->post('hastames');   
        //$periodo=$this->input->post('periodo');
        $procesos=$this->input->post('procesos');
        $poronapre=$this->input->post('poronapre');
        $pordianca=$this->input->post('pordianca');
        $tituloconsulta.=" P({$procesos})";
        $tituloconsulta.=" A({$año})";
        $tituloconsulta.=" M({$desdemes}-{$hastames})";
        $datos['tituloconsulta']=$tituloconsulta;
        $datos['poronapre']=$poronapre;
        $datos['pordianca']=$pordianca;
        switch($this->input->post('grupovistas')){
            case '4':
                $resultado=$this->ccdh_modelo->get_porficha($año,$desdemes,$hastames,$procesos,$conceptos,$tiposnominas,$poronapre,$pordianca,$fichas,$talleres); 
                $datos['resultado']=$resultado;
                $this->load->view('previsualizar_porficha',$datos);
                break;
            case '3':
                $resultado=$this->ccdh_modelo->get_porperiodo($año,$desdemes,$hastames,$procesos,$conceptos,$tiposnominas,$poronapre,$pordianca,$fichas,$talleres); 
                $datos['resultado']=$resultado;
                $this->load->view('previsualizar_porperiodo',$datos);
                break;
            case '2':
                $resultado=$this->ccdh_modelo->get_pormes($año,$desdemes,$hastames,$procesos,$conceptos,$tiposnominas,$poronapre,$pordianca,$fichas,$talleres); 
                $datos['resultado']=$resultado;
                $this->load->view('previsualizar_pormes',$datos);
                break;
            case '1':
                $resultado=$this->ccdh_modelo->get_porconcepto($año,$desdemes,$hastames,$procesos,$conceptos,$tiposnominas,$poronapre,$pordianca,$fichas,$talleres); 
                $datos['resultado']=$resultado;
                $this->load->view('previsualizar_porconcepto',$datos);
                break;            
        }
        
       
        $resultado->free_result();
    }
    private function preparar_fichas($fichas){
        $fichas= explode(',', $fichas);
        foreach($fichas as $i => $ficha){
            $fichas[$i]=trim($fichas[$i]);
            $fichas[$i]= substr($fichas[$i], 0,1).'      '.substr($fichas[$i], 1);
        }
        $fichas= implode(',', $fichas);
        $fichas=trim($fichas);
        return $fichas;
    }
    public function salir(){
        $esta_autenticado=$this->session->esta_autenticado;        
        $esta_autenticado[$this->ruta]=false;
        $this->session->esta_autenticado=$esta_autenticado;
        //$this->session->unset_userdata('ConsultaPrincipal');
    }
    private function ExpandirRango($rango=null){
       if($rango===null){
            return "";
        }
        $arreglo=explode("-", $rango);
        if(empty($arreglo) || count($arreglo)===0){
            return "";
        }elseif(count($arreglo)===1){
            return $arreglo[0];
        }
        $inicio= (int)explode("-", $rango)[0];
        $final= (int)explode("-", $rango)[1];
        if($inicio>$final){
            $final= (int)explode("-", $rango)[0];
            $inicio= (int)explode("-", $rango)[1];
        }
        $resultado="";
        for($i=$inicio;$i<=$final;$i++){
            if($i===$inicio){
                $resultado.=$i;            
            }else{
                $resultado.=", ".$i;
            }
        }
        return $resultado;
    }
    private function ExpandirConceptos($conceptos=null){
        if($conceptos===null){
            return "";            
        }  
        $arreglo= explode(",", $conceptos);
        foreach($arreglo as $i=>$valor){
            $arreglo[$i]=trim($arreglo[$i]);
            $arreglo[$i]= $this->ExpandirRango($arreglo[$i]);
            if(empty($arreglo[$i])){
                unset($arreglo[$i]);                
            }
        }
        $resultado= implode(", ", $arreglo);
        return $resultado;
    }
}
