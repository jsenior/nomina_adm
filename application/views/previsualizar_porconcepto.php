<div class="row">  

    <div class="col text-light bg-primary bg-gradient mx-2  rounded-top">
        <div class="row">
            <div class="col p-0 m-0 me-auto ms-lg-1">
                <h5 id="tipoConsulta" class="bg-primary bg-gradient text-light text-center m-0 p-2 rounded-top"><b id="tituloConsulta"><?="[{$tituloconsulta}]"?></b></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-auto p-0 m-0 mx-auto mx-md-0 me-md-2">
                <!--<div class="row">-->
                    <div class="input-group m-1">                        
                        <span class="input-group-text bg-primary bg-gradient text-white-50 border p-2" style="border-color: rgba(255,255,255,0.5)!important;"><i class="fa fa-filter" style="font-size: 0.67rem; "></i></span>
                        <input id="filtro" type="text" class="col-auto form-control border-start-0" placeholder="Filtrar Resultado" style="font-size: 0.75rem; " title="Presiona [Enter] ó [Esc] para mostrar la tabla completa ó escribe un dato para reducir la tabla y resaltar las coincidencias." data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"/>
                    </div>
                <!--</div>-->
            </div>
            
            <div class="col-auto p-0 m-0 mx-auto mx-md-0"> 
                <!--mx-auto ms-lg-0 me-md-1-->
                <ul class="pagination pagination-sm pager p-0 m-1 justify-content-start" id="myPager"></ul>
            </div>
            
            <div class="col-auto p-0 m-0 mx-auto mx-md-0" >
                <div class="btn-group btn-group-sm m-1" role="group">
                    
                    <button id="btnDescargarXLSX" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Descargar XLSX de Excel"><i class="fas fa-download fa-fw mx-1"></i>XLSX</button>
                    <button id="btnDescargar" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Descargar CSV de Excel"><i class="fas fa-download fa-fw mx-1"></i>CSV</button>
                    <button id="btnDescargarTXT" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Descargar TXT de Notepad"><i class="fas fa-download fa-fw mx-1"></i>TXT</button>
                    <button id="btnDescargarPDF" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Descargar PDF de Acrobat"><i class="fas fa-download fa-fw mx-1"></i>PDF</button>
                    <button id="btnCopiar" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Copiar datos al Portapapeles"><i class="fas fa-copy fa-fw mx-1"></i>COPIAR</button>
                    
                </div>                
            </div>
            <div class="col-auto p-0 m-0 mx-auto mx-md-0" >
                <div class="btn-group btn-group-sm m-1" role="group">
                    <button id="btnFullScreen" class="btn btn-sm btn-primary border-light" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="Ver en Pantalla Completa"><i class="fas fa-expand-arrows"></i></button>
                </div>                
            </div>
            
        </div>
    </div>       

</div>
    
<div class="row">
    <div class="col table-responsive h-auto mx-2" > <!--style=" height: 100%;"-->

                <?php
                    $this->table->set_template(array('table_open'=>'<table id="tblConsulta" class="table table-hover bg-light text-center small m-0" style="font-size: 0.75rem; ">'));
                    $this->table->template['heading_cell_start']='<th class="alert-primary bg-primary bg-gradient text-light align-middle p-1" style="min-width: 20px;">';
                    $this->table->template['heading_row_start']='<tr style="font-size: 0.75rem; line-height: 0.75rem;">';
                    $this->table->template['tbody_open']='<tbody id="cuerpoTabla">';
                    $this->table->template['cell_start']='<td class="align-middle p-0 m-0 tableexport-string target">';//'<td class="px-4 m-0">';PARA CAMBIAR EL ESTILO EN CADA CELDA PARTICULAR
                    $this->table->template['cell_alt_start']=$this->table->template['cell_start'];
                    $this->table->set_heading(
                            "AÑO","CONCEPTO","DESCRIPCION","ASIGNACIONES","DEDUCCIONES","APORTE PATRONAL","MONTO ONAPRE<br>".$poronapre."%","MONTO DIANCA<br>".$pordianca."%"
                    );
                    $this->table->template['thead_open']='<thead class="alert-primary bg-primary bg-gradient text-white align-middle p-0 m-0">';
                    $resultado=$resultado->result_array();
                    $hCargadas=0;
                    $hPorcasrgar=0;
                    $hSobrecargadas=0;
                    $tVacaciones=0;
                    foreach($resultado as $i=>$fila){                        
                        foreach($fila as $key=>$value){
                            
                            if(is_numeric($value) && $key!=='FICHA' ){
                                if(strpos($value,".")!==false){    
                                        $resultado[$i][$key]= number_format($value,2,",",".");    
                                }else{
                                    $resultado[$i][$key]= number_format($value,0,".","");
                                }
                                if($key=='CONCEPTO'){
                                    $resultado[$i][$key]= str_pad($resultado[$i][$key],3, '0', STR_PAD_LEFT);
                                }
                            }else{
                                $resultado[$i][$key]= utf8_encode(trim($value));
                            }
                        }
                        //$resultado[$i]['EXCLUIR']='<div class="d-flex form-check form-switch justify-content-center align-items-center m-0 p-0"><input id="excluir" class="form-check-input form-check-input-red m-0 p-0" style="font-size:0.875rem;" type="checkbox" value="" onchange="//if($(this).prop(\'checked\')){$(this).parent().prepend(\'<label>¡excluido!</label>\').find(\'label\').addClass(\'mx-2 text-danger\');}else{$(this).parent().find(\'label\').remove();}" onmouseenter="$(\'.tooltip-inner\').addClass(\'bg-light bg-gradient text-danger\').css(\'border-radius\',\'20px\');" title="<h5><b>Excluir esta fila</b></h5><b><i class=\'text-body\'>ACTIVAR PARA EXCLUIR</i></b>" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true"></div>';
                    }
                    echo $this->table->generate($resultado);

                ?> 

            <script class="code-hide">
                //SE MOVIO A LA DEFINICION DE LA PLANTILLA DE LA TABLA
//                function setTableExportCellDataTypes(){
//                    var tabla=$("#tblConsulta");
//                    var filas=tabla.find("tbody tr");
//                    for(var i=0;i<filas.length;i++){  
//                        var col_monto=0;
//                        var columnas=$(filas[i]).find("td");
//                        for(var j=0;j<columnas.length;j++){
//                            if(j===col_monto){
//                                $(columnas[j]).addClass("tableexport-general target");
//                            }else{
//                                $(columnas[j]).addClass("tableexport-string target");
//                            }
//                        }
//                    }
//                }
                setTimeout(function(){
                    $('#cuerpoTabla').pageMe(
                                {
                                    pagerSelector:'#myPager',
                                    showPrevNext:true,
                                    showFirstLast:true,
                                    hidePageNumbers:true,
                                    perPage:14,
                                    childSelector:"tr:not([style='display:none'])"                                    
                                }       
                            );
                    activarToolTips();
                    
                    //setTableExportCellDataTypes();
                    
                    $('.code-hide').remove();
                    var configuracion={
                        theme : "bootstrap",
                        headerTemplate:'<div class="row justify-content-center align-items-center p-0 m-0"> <div class="col-auto p-0 m-0 ms-1 me-1">{content}</div> <div class="col-auto p-0 m-0"> {icon}</div></div>',
//                        cssIcon:"fas fa-sort",
                        cssIconAsc:"fad fa-sort-up fa-2x",
                        cssIconDesc:"fad fa-sort-down fa-2x",
//                        cssIconDisabled:"",
//                        cssIconNone:"",
                        headers:{
                            '.noSort':{
                                sorter:false
                            }
                        }
                    };                    
                    $('#tblConsulta').tablesorter(configuracion);
                    
                    $("#btnCopiar").click(btnCopiar_click);
                    $('#btnDescargar').click(btnDescargar_click);
                    $('#btnDescargarXLSX').click(btnDescargarXLSX_click);
                    $('#btnDescargarPDF').click(btnDescargarPDF_click);
                    $('#btnDescargarTXT').click(btnDescargarTXT_click);
                    $("#btnFullScreen").click(btnFullScreen_click);
                    $('#btnDescargarXLSX').focus();
                },1);
            </script>
            
        </div>
    
</div>
<!--<div class="row">
    <table class="table col col-sm-auto w-auto table-hover  bg-light text-start small m-2 mx-sm-auto rounded-top" style="font-size: 0.75rem; ">
        <thead>
            <tr ><th class="alert-primary bg-primary bg-gradient text-light align-middle p-1 rounded-top  fs-6"><i class="fa fa-info-circle fa-fw"></i> LEYENDA</th></tr>
        </thead>

        <tbody>
            <tr class="alert-success text-success"><td class="p-1 px-3 pt-0">FILA CON HORAS QUE FALTAN POR CARGAR. <b class="fs-6">(<?=$hPorcasrgar?> trabajadores)</b></td></tr>
            <tr class="alert-primary text-primary"><td class="p-1 px-3 pt-0">FILA CON HORAS QUE YA ESTÁN CARGADAS. <b class="fs-6">(<?=$hCargadas?> trabajadores)</b></td></tr>
            <tr class="alert-warning"><td class="p-1 px-3 pt-0">FILA CON TRABAJADOR DE VACACIONES. <b class="fs-6">(<?=$tVacaciones?> trabajadores)</b></td></tr>
            <tr class="alert-danger text-danger"><td class="p-1 px-3 pt-0">FILA QUE PASA DE 40 Y 44 HORAS CARGADAS. <b class="fs-6">(<?=$hSobrecargadas?> trabajadores)</b></td></tr>
        </tbody>
    </table>
</div>-->