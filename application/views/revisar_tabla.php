<?php 
$reporte=$reporte->result_array();
//print_r($reporte); 
$this->table->set_template(array('table_open'=>'<table id="tblConsulta" class="table table-hover bg-light text-center small m-0" style="font-size: 0.75rem; ">'));
$this->table->template['heading_cell_start']='<th class="alert-primary bg-primary bg-gradient text-light align-middle p-1" style="min-width: 20px;">';
$this->table->template['heading_row_start']='<tr style="font-size: 0.75rem; line-height: 0.75rem;">';
$this->table->template['tbody_open']='<tbody id="cuerpoTabla">';
$this->table->template['cell_start']='<td class="align-middle p-0 m-0 tableexport-string target">';//'<td class="px-4 m-0">';PARA CAMBIAR EL ESTILO EN CADA CELDA PARTICULAR
$this->table->template['cell_alt_start']=$this->table->template['cell_start'];
$this->table->set_heading("TPROCE","TIPNOM","CLASE", "PER012", "USER12", "WI12", "FICH12", "CLAV12", "CAN012", "BOL012", "FECM12", "DPTO12", "MAY012");
echo $this->table->generate($reporte);
?>

<script>
    setTimeout(() => {
        var filas = $("#tblConsulta tbody").find('tr');
    for (var i=0; i< filas.length;i++){
        var cols = $(filas[i]).find('td');
        var mayo12 = $(cols[12]).text();
        console.info(mayo12);
        var rojo = mayo12.substr(0,5)+'<span style="font-weight:bold;color:red">'+mayo12.substr(6,4)+'</span>'+mayo12.substr(10,4);
        $(cols[12]).html(rojo);
    }
    }, 100);
    

</script>