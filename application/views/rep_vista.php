<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Modulo Consulta Nomina-Administracion</title>
</head>
<body>

<div class="col table-responsive h-auto mx-2">
	<h1>Modulo de Consulta Cierre Nomina a Contabilidad Test 2</h1>
	<p>Solo Aplica a Periodos Abiertos.</p>

	<form id="formularioConsulta" name="formularioConsulta" class="form-inline" method="POST" >

	<div id="grupotipnom" class="d-flex">
		<label>Tipo Nomina</label><br>
		<?php foreach($tipnom as $i => $fila){ ?> 
			<div class="col-auto px-1 d-flex align-items-center"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="<?=$fila["DESTIPNOM"]?>">
                <input type="checkbox" name="tiponomina_<?=$fila["TIPNOM"]?>" id="tiponomina_<?=$fila["TIPNOM"]?>" class="form-check-input me-1 ms-1" <?=eval('return isset($'."tiponomina_".$fila["TIPNOM"].");")==true?"checked":""?>>
                <label class="form-check-label font-monospace small mt-1 me-2" for="tiponomina_<?=$fila["TIPNOM"]?>"><?=$fila["TIPNOM"]?></label></input>
            </div>
        <?php //if(is_numeric($fila["TIPNOM"]) && !is_numeric($tipnom[$i+1]["TIPNOM"])){echo "<br>";}?>
        <?php } ?>   
        	<div class="col-auto px-1 d-flex align-items-center">
                <input type="checkbox" name="todostipnom" id="todostipnom" class="btn-check me-1 ms-2" <?=isset($todostipnom)?"checked":""?>>
                <label class="btn btn-outline-primary btn-sm m-0 p-0 px-1 mt-1 ms-1" for="todostipnom" data-bs-toggle="tooltip" data-bs-placement="top" title="Marca o Desmarca Todo"><div class="d-flex align-items-center"><i class="fa fa-check me-1" style="font-size: 9px"></i>Todas</div></label></input>
        	</div>
	
	</div>

	<div id="body">
		<br>
		<button id="btnConsultar" type="button" class="btn btn-primary"><i class="fas fa-play fa-fw"></i> Consultar</button>
	</div>
	</form> 
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

<div class="row justify-content-center">
    <div id="previsualizar" class="col mb-1">

    </div> 
</div>
</body>
</html>