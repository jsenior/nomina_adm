            <div class="row justify-content-center">
                <div class="card col   border border-2 border-primary shadow m-2 p-0" style="border-radius: 10px;  max-width:540px;"  >
                    <h5 class="text-center text-white bg-primary bg-gradient py-1 pb-2" style="border-radius: 7px 7px 0 0;">Parámetros de la Consulta</h5>
                    <div class="row px-2 pb-2">
                        <form id="formularioConsulta" name="formularioConsulta" class="form-inline" method="POST" > 
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                                <div class="col-12 col-sm-auto" style="min-width:90.45px;" ><label class="form-control form-control-sm form-label fw-bold" for="">Nóminas:</label></div>
                                <div class="col">
                                    <div class="col border rounded-3 py-1 pe-3 ps-3"> 
                                        <div class="row" id="grupotipnom">
                                        <?php foreach($tipnom as $i => $fila){ ?>  
                                            <div class="col-auto px-1 d-flex align-items-center"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="<?=$fila["DESTIPNOM"]?>">
                                                <input type="checkbox" name="tiponomina_<?=$fila["TIPNOM"]?>" id="tiponomina_<?=$fila["TIPNOM"]?>" class="form-check-input me-1 ms-1" <?=eval('return isset($'."tiponomina_".$fila["TIPNOM"].");")==true?"checked":""?>>
                                                <label class="form-check-label font-monospace small mt-1 me-2" for="tiponomina_<?=$fila["TIPNOM"]?>"><?=$fila["TIPNOM"]?></label></input>
                                            </div>
                                            <?php //if(is_numeric($fila["TIPNOM"]) && !is_numeric($tipnom[$i+1]["TIPNOM"])){echo "<br>";}?>
                                        <?php } ?>   
                                            <div class="col-auto px-1 d-flex align-items-center">
                                                <input type="checkbox" name="todostipnom" id="todostipnom" class="btn-check me-1 ms-2" <?=isset($todostipnom)?"checked":""?>>
                                                <label class="btn btn-outline-primary btn-sm m-0 p-0 px-1 mt-1 ms-1" for="todostipnom" data-bs-toggle="tooltip" data-bs-placement="top" title="Marca o Desmarca Todo"><div class="d-flex align-items-center"><i class="fa fa-check me-1" style="font-size: 9px"></i>Todas</div></label></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                                <div class="col-12 col-sm-auto pe-0" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="procesos">Procesos:</label></div>
                                <div class="col"> 
                                    <input id="procesos" name="procesos" type="text" class="form-control form-control-sm" value="1, 3, 5, 9, 10, 12, 13" placeholder="Ej: 1, 4, 8 puede separar con   .  ,  ;  -  +   [espacio]   ó   [enter]" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="<b>Valores Posibles:</b><br>1, 3, 4, 5, 8, 9, 10, 12, 13"/>
                                </div>
                                
                                <div class="col-12 col-sm-auto pe-0" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold text-end" for="ano">Año:</label></div>
                                <div class="col"> 
                                    <input id="ano" name="ano" type="number" min="2010" max="<?=((int)date("Y"))?>" value="<?=((int)date("Y"))?>" class="form-control form-control-sm" />
                                </div>
                                
<!--                                <div class="col-12 col-sm-auto pe-0" > 
                                    <label class="form-control form-control-sm form-label fw-bold px-0 text-end" for="periodo">Periodo:</label>
                                </div>
                                <div class="col"> 
                                    <input id="periodo" name="periodo" type="number" min="1" max="99" value="" class="form-control form-control-sm" />
                                </div> -->
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                                <div class="col-12 col-sm-auto pe-0" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="desdemes">Desde Mes:</label></div>
                                <div class="col"> 
                                    <input id="desdemes" name="desdemes" type="number" min="1" max="12" inputmax="hastames" value="<?=((int)date("m"))?>" class="form-control form-control-sm" />
                                </div>
                                <div class="col-12 col-sm-auto pe-0" > <label class="form-control form-control-sm form-label fw-bold px-0 text-end" for="hastames">Hasta Mes:</label></div>
                                <div class="col"> 
                                    <input id="hastames" name="hastames" type="number" min="1" max="12" inputmin="desdemes" value="<?=((int)date("m"))?>" class="form-control form-control-sm" />
                                </div>
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                                <div class="col-12 col-sm-auto" style="min-width:90.45px;"> 
                                    <label class="form-control form-control-sm form-label fw-bold" for="talleres">Conceptos:</label>
                                    
                                </div>
                                <div class="col col-auto ps-0">  
                                    <a onclick="$('#grupoconceptos').val('1'); $('#grupoconceptos').change();" style="cursor: pointer"><i class="fad fa-sync fa-fw p-0 m-0"></i></a>                                 
                                </div> 
                                <div class="col ps-0">   
                                    <select id="grupoconceptos" name="grupoconceptos" class="form-select form-select-sm">
                                        <option value="1">Deducciones (Parafiscales)</option>
                                        <option value="2">Deducciones (NO Parafiscales)</option>                                         
                                        <option value="3">Deducciones (Todas)</option>  
                                        <option value="4">Asignaciones (Todas)</option> 
                                        <option value="5">Asignaciones (se procesan en Nómina)</option> 
                                        <option value="6">Asignaciones (se procesan en Anticipos)</option> 
                                        <option value="7">Asignaciones (se procesan en Nómina Especial)</option> 
                                        <option value="8">Asignaciones (se procesan en Vacación Individual)</option> 
                                        <option value="9">Asignaciones (se procesan en Vacación Colectiva)</option> 
                                        <option value="10">Asignaciones (se procesan en Pago Intereses)</option> 
                                        <option value="11">Asignaciones (se procesan en Utilidades)</option> 
                                        <option value="12">Asignaciones (se procesan en Prestaciones)</option> 
                                        <option value="13">Carta al Banco (Sueldo Básico y Descuentos)</option> 
                                        <option value="14">Carta al Banco (Horas Extras y Sobretiempos)</option> 
                                        <option value="15">Carta al Banco (Bonificación)</option> 
                                        <option value="16">Concepto 023 (Conceptos Laborales)</option> 
                                        <option value="17">Concepto 023 (Solo Salario Básico)</option> 
                                        <option value="18">Concepto 023 (Sin Salario Básico)</option> 
                                        <option value="19">Concepto 023 (Conceptos Vacacionales)</option> 
                                        <option value="20">Personalizada</option> 
                                    </select>
                                    <textarea class="form-control form-control-sm" id="conceptos" name="conceptos" rows="1" placeholder="Ejemplo de lista: 1,2,3,4,5 | Ejemplo de rango: 1-5 | Puede separar con (.) (,) (;) ([espacio]) ó ([enter])" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" data-bs-html="true" title="<b>Ejemplos:</b><br><b>Lista:</b> 010, 700-790, 940<br><b>Rango: </b>160-190<br>Puede separar con ( . ) ( , ) ( ; ) [espacio] ó [enter]">700, 760, 770, 782, 790</textarea>
                                </div> 
                                                               
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                            <div class="col-12 col-sm-auto" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="fichas">Fichas:<br><i class="fw-normal text-secondary">(opcional)</i></label></div>
                                
                                <div class="col ps-1"> <textarea class="form-control form-control-sm" id="fichas" name="fichas" rows="1" value="" placeholder="Ejemplo: G003, 1234, R012, puede separar con   .   ,   ;   [espacio]   ó   [enter], deje en blanco para todas las fichas"></textarea></div>                                  
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                            <div class="col-12 col-sm-auto" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="talleres">Talleres:<br><i class="fw-normal text-secondary">(opcional)</i></label></div>
                                
                                <div class="col ps-1"> <textarea class="form-control form-control-sm" id="talleres" name="talleres" rows="1" value="" placeholder="Ejemplo: 4610, 4570, 4710, puede separar con   .   ,   ;   [espacio]   ó   [enter], deje en blanco para todos los talleres"></textarea></div>                                  
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">                                
                                <div class="col-12 col-sm-auto pe-0" > <label class="form-control form-control-sm form-label fw-bold px-0 text-end" for="porpatria">% ONAPRE:</label></div>
                                <div class="col"> 
                                    <input id="poronapre" name="poronapre" type="number" min="0" max="100" step="0.01" value="0" inputbalance="pordianca" class="form-control form-control-sm" />
                                </div>
                                <div class="col-12 col-sm-auto pe-0" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="pordianca">% DIANCA:</label></div>
                                <div class="col"> 
                                    <input id="pordianca" name="pordianca" type="number" min="0" max="100" step="0.01" value="100" inputbalance="poronapre" class="form-control form-control-sm" />
                                </div>
                            </div>
                            <div class="row m-1 border-bottom pb-1" style="border-bottom-color: #ced4da;">
                                <div class="col-12 col-sm-auto" style="min-width:90.45px;"> <label class="form-control form-control-sm form-label fw-bold" for="talleres">Tipo de Consulta:</label></div>
                                <div class="col ps-0">   
                                    <select id="grupovistas" name="grupovistas" class="form-select form-select-sm">
                                        <option value="1">Resumida por Concepto</option>                                          
                                        <option value="2">Resumida por Mes</option>  
                                        <option value="3">Resumida por Periodo</option>
                                        <option value="4">Detallada por Ficha (Puede tardar más de 10 min.)</option>
                                    </select>
                                </div>
                            </div>    
                            
                            
                            <div class="row m-1 " >
                                <div class="d-grid col d-sm-flex col-sm-auto gap-2 mx-auto mt-2">
                                    <button id="btnConsultar" type="button" class="btn btn-primary"><i class="fas fa-play fa-fw"></i> Consultar</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div> 
            <div class="row justify-content-center">
                <div id="previsualizar" class="col mb-1">

                </div> 
            </div>