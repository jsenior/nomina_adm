<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Control de Acceso (Administrador de Usuarios)</title>
        <link rel="stylesheet" href="/bootstrap-5.0.1-dist/css/bootstrap.min.css" >    
        <link rel="stylesheet" href="/fontawesome-free-5.15.3-web/css/all.min.css" >
        <link rel="stylesheet" href="/CI3CAMD/css/camd.css" > 
        <script src="/jquery-3.6.0/jquery-3.6.0.min.js"></script>
        <script src="/jquery-3.6.0/jquery.highlight-5.js"></script>
        <script src="/CI3CAMD/js/paginacionjquery.js"></script>
        <script src="/popper-umd-2.4.0/popper.min.js"></script>
        <script src="/bootstrap-5.0.1-dist/js/bootstrap.min.js"></script>
        <script src="/CI3CAMD/js/usuarios.js"></script>    </head>
    <body>
        <div id="modal-spinner" class="fixed-top bg-dark justify-content-center align-items-center" style="display:none!important; position: fixed; min-width: 100%; min-height: 100%; opacity: 0.85; z-index:5000;">
        <!--div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog"-->        
                    <div class="row justify-content-center align-items-center">
                        <div class="col-auto">
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.500s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.375s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.250s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.125s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem;"></div>
                        </div>
                        <div class="col-auto">
                            <div class="col-auto text-secondary text-center"><h1>espere por favor...</h1></div>
                        </div>
                        <div class="col-auto">
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.125s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.25s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.375s;"></div>
                            <div class="col-auto spinner-grow text-secondary m-2 align-self-middle" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.500s;"></div>
                        </div>                        
                    </div>
            <!--/div-->
        </div>
        <div class="container-fluid p-0" >
            <div class="container-fluid p-1 bg-dark bg-gradient text-white invisible">
                <h2 class="text-center w-100">
                    <a class="text-white text-decoration-none" href="https://127.0.0.1/CI3CAMD/">
                        Control de Acceso (Administrador de Usuarios)                    </a>
                </h2>
            </div>
            <div class="container-fluid p-1 bg-dark bg-gradient text-white fixed-top shadow">
                <h2 class="text-center w-100">
                    <a class="text-white text-decoration-none" href="https://127.0.0.1/CI3CAMD/">
                        Control de Acceso (Administrador de Usuarios)                    </a>
                </h2>
            </div>
            
            

<div id="ContenedorPrincipal" class="row justify-content-evenly align-items-center p-0 m-0 h-100">
    <div class="col-11">
        <div class="row m-2" >
            <div class="col text-center">
                <button type="button" id="btnCrear" class="col btn btn-dark text-orange border border-secondary mx-1" style="min-width: 120px;"><i class="fa fa-user-plus me-2"></i>Crear</button>
                <button type="button" id="btnEditar" class="col btn btn-dark text-orange border border-secondary mx-1" style="min-width: 120px;"><i class="fa fa-user-edit me-2"></i>Editar</button>
                <button type="button" id="btnConsultar" class="col btn btn-dark  border border-secondary mx-1" style="min-width: 120px;"><i class="fa fa-search me-2" ></i>Consultar</button>
            </div>            
        </div>
        <div id="ventana" class="row">
            <div style="min-height: 408px;"><!--para ajustar la altura y que los botones no se muevan-->
    <form id="formularioConsultar" method="post" class="col bg-dark bg-gradient rounded-10 form text-orange border border-secondary px-3 pt-2 pb-0 m-auto shadow" style="max-width: 500px;" >
        <h3><!--i class="fa fa-user mx-1 me-2 "></i--><i class="fa fa-search mx-1 me-2 "></i>Consultar Usuario</h3>

        <div class="row m-0 mb-1">
            <div class="col-12 col-sm-12 p-0 px-1">
                <label class="small">Cualquier dato:</label>
                <input type="text" id="datodebusqueda" name="datodebusqueda" class="form-control p-1 pt-0" placeholder="Ingrese un dato y presione enter"/>
            </div>            
        </div>        
        <div class="row text-center my-3">
            <div class="col">
                <button id="btnBuscar" type="button" class="btn btn-dark text-orange mx-1 border border-secondary"><i class="fa fa-search me-2"></i>Buscar</button>
            </div>
        </div>
    </form>
    <div id="resultadoConsulta" class="row">
            
    </div>
</div>
<script>
    setTimeout(document_ready_consultar,0);
</script> 
        </div>
        
    </div> 
</div>

                
                <div class="container-fluid py-1 m-0 bg-dark bg-gradient text-white invisible">
                    <h6 class="text-center text-white align-items-center m-0" style="font-size: 8pt;"><img src="https://127.0.0.1/CI3CAMD/img/logo4.png" class="d-inline-block m-0" height="32px"/> DIANCA, C.A. G-20007556-2 | Gerencia de AIT | Módulo: CAMD | Desarrollador: Ing. Raúl González</h6>
                </div>
                <div class="container-fluid py-1 m-0 bg-dark bg-gradient text-white fixed-bottom ">
                    <h6 class="text-center text-white align-items-center m-0" style="font-size: 8pt;"><img src="https://127.0.0.1/CI3CAMD/img/logo4.png" class="d-inline-block m-0" height="32px"/> DIANCA, C.A. G-20007556-2 | Gerencia de AIT | Módulo: CAMD | Desarrollador: Ing. Raúl González</h6>
                </div>
                
            </div>  
        
    </body>
</html>