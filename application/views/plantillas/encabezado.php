<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo strip_tags($titulo);?></title>
        <link rel="stylesheet" href="/bootstrap-5.3.2-dist/css/bootstrap.min.css" >    
        <link rel="stylesheet" href="/fontawesome-pro-5.15.3-web/css/all.min.css" >
        <!--<link rel="stylesheet" href="<?=$ruta?>css/ccdh.css" >--> 
        <script src="/jquery-3.6.0/jquery-3.6.0.min.js" class="code-hide"></script>
        <script src="/jquery-3.6.0/jquery.highlight-5.js" class="code-hide"></script>
        <script src="/jquery-3.6.0/jquery-paginacion-rg.js" class="code-hide"></script>
        <script src="/jquery-3.6.0/jquery.tablesorter.min.js" class="code-hide"></script>
        <script src="/popper-umd-2.4.0/popper.min.js" class="code-hide"></script>
        <script src="/bootstrap-5.3.2-dist/js/bootstrap.min.js" class="code-hide"></script>
        <!--LIBRERIAS PARA EXPORTAR A EXCEL-->
        <script src="/xlsx.js/xlsx.full.min.js" ></script>
        <script src="/FileSaver-1.3.6/FileSaver.min.js" ></script>
        <script src="/TableExport-5.2.0/tableexport.min.js" ></script>
        <!--LIBRERIAS PARA EXPORTAR A PDF-->
        <script src="/es6-promise-4.2.8/es6-promise.auto.min.js" ></script>
        <script src="/jspdf-2.5.1/jspdf.umd.min.js" ></script>
        <script src="/html2canvas-1.4.1/html2canvas.min.js" ></script>
        <script src="/html2pdf-0.10.1/html2pdf.min.js" ></script>
        
        <!--<script src="</*?=$ruta?>jspdf-2.5.1/jspdf.umd.min.js" ></script>-->
        <?=$javascript?>
    </head>
    <body>
        <?php $this->view("plantillas/modal-spinner-dianca");?>    
        
        <!--INVISIBLE PARA PRESERVAR EL ESPACIO EN EL DISEÑO DE LA PAGINA-->
        <!----><nav class="navbar navbar-expand-sm bg-dark navbar-dark mb-2 shadow invisible">
        <!---->    <div class="container-fluid">
        <!---->        <a class="navbar-brand" href="#"><i class="fad fa-project-diagram me-2"></i><?php echo $titulo; ?></a>
        <!---->        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
        <!---->            <span class="navbar-toggler-icon"></span>
        <!---->        </button>
        <!---->        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <!---->            <ul class="navbar-nav ms-auto">                        
        <!---->                <li class="nav-item">                            
        <!---->                    <div class="dropdown">
        <!---->                        <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton1falso" data-bs-toggle="dropdown" aria-expanded="false">
        <!---->                            <i class="fad fa-user me-2"></i> <?=$nombre_usuario?>
        <!---->                        </button>
        <!---->                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">                                    
        <!---->                            <li><button id="btnSalirfalso" class="dropdown-item"><i class="fa fa-sign-out-alt me-1"></i><b>Cerrar Sesión</b></button></li>
        <!---->                        </ul>
        <!---->                    </div>
        <!---->                </li>   
        <!---->            </ul>
        <!---->        </div>                    
        <!---->    </div>
        <!----></nav><!--INVISIBLE PARA PRESERVAR EL ESPACIO EN EL DISEÑO DE LA PAGINA-->        
        <nav id="Encabezado" class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark shadow">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?=$ruta?>"><i class="fad fa-project-diagram me-2"></i><?php echo $titulo; ?></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav ms-auto">                        
                        <li class="nav-item">                            
                            <div class="dropdown">
                                <button class="btn btn-dark dropdown-toggle text-orange" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fad fa-user me-2"></i> <?=$nombre_usuario?>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton1">                                    
                                    <li><button id="btnSalir" class="dropdown-item"><i class="fa fa-sign-out-alt me-1"></i><b>Cerrar Sesión</b></button></li>
                                </ul>
                            </div>
                        </li>   
                    </ul>
                </div>                    
            </div>
        </nav>
        <!--CONTENEDOR PRINCIPAL (con cierre en el pie de pagina)-->
        <div class="container-fluid" >