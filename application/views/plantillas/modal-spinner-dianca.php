        <style> 
            :root{/*VARIABLES*/
                --rad:5;/*RADIO*/
                --dia:calc(var(--rad) * 2);/*DIAMETRO*/
                --vel:2;/*VELOCIDAD*/
                --zom:1;/*ZOOM*/ /*1=100%*/
                --bor:0.33333333;/*0.375;/*BORDE*/           
            }
                     
            #modal-spinner{                 
                background-color: #000000aa;                 
                display:none;                
                position:fixed;
                top:0;
                left:0;
                align-items: center;
                justify-content: center;
                margin: 0;
                padding: 0;  
                min-width: 100vw; 
                min-height: 100vh;                 
                z-index:5000;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }
            .spinner-lights{   
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh - var(--dia)) / 2);
                border-style: solid;
                border-color: transparent;                            
                background-image: url('<?=base_url('img/estrella16.png')?>');
                background-position: center;
                background-size: 100%;
                background-repeat:no-repeat;
                border-width: 0;
                border-radius: calc(var(--dia) * 1rem * var(--zom));
                width: calc(var(--dia) * 1.75rem * var(--zom));
                height: calc(var(--dia) * 1.75rem * var(--zom));
                animation: 2s linear infinite brillar; 
            }
            .spinner-out{   
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh - var(--dia)) / 2);
                border-style: solid;
                border-color: orangered; 
                border-right-color: transparent;
                border-left-color: transparent;    
                background-position: center;
                background-size: 100%;
                background-repeat:no-repeat;
                border-width: calc(var(--bor) * 1rem * var(--zom));
                border-radius: calc(var(--dia) * 1rem * var(--zom));
                width: calc(var(--dia) * 1rem * var(--zom));
                height: calc(var(--dia) * 1rem * var(--zom)); 
                animation: calc(4s / var(--vel)) linear infinite spinner-border-right;
            }
            .spinner-mid{
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh - var(--dia)) / 2);
                border-style: solid;                
                border-color: black;                
                border-right-color: transparent;
                border-left-color: transparent;                   
                border-width: calc(var(--bor) * 1rem * var(--zom));
                border-radius: calc((var(--dia) - var(--bor) * 2) * 1rem * var(--zom));
                width: calc((var(--dia) - var(--bor) * 2) * 1rem * var(--zom));
                height: calc((var(--dia) - var(--bor) * 2) * 1rem * var(--zom));
                animation: calc(2s / var(--vel)) linear infinite spinner-border-left;
            }
            .spinner-in{
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh - var(--dia)) / 2);
                border-style: solid;   
                border-color: silver;                
                border-right-color: transparent;
                border-left-color: transparent;                
                border-width: calc(var(--bor) * 1rem * var(--zom));
                border-radius: calc((var(--dia) - var(--bor) * 6) * 1rem * var(--zom));
                width: calc((var(--dia) - var(--bor) * 4) * 1rem * var(--zom));
                height: calc((var(--dia) - var(--bor) * 4) * 1rem * var(--zom));
                animation: calc(2s / var(--vel)) linear infinite spinner-border-right;
            }
            .spinner-logo{  
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh - var(--dia)) / 2);
                border-radius: calc((var(--dia) - var(--bor) * 4) * 1rem * var(--zom));
                width: calc((var(--dia) - var(--bor) * 2) * 1rem * var(--zom));
                height: auto;
            }
            .latido{                
                animation: 1s linear infinite parpadeo-latido;                 
            } 
            .mensaje{
                position:absolute;
                left:calc((100vw - var(--dia)) / 2);
                top:calc((100vh / 2) + (var(--rad) * 1rem * var(--zom)));
                font-family: Arial;
                font-weight: normal;
                font-size: calc((var(--dia) / 8) * 1rem * var(--zom));                
                max-width: 67vw;
                margin:0;
                padding-top: calc(0.5rem * var(--zom));
                text-align: center;
                color: white;
                text-shadow: 1px 1px 3px black,1px 1px 3px black,1px 1px 3px black;    
                
            }
            @keyframes spinner-light {
                to {
                    transform: rotate(360deg);
                }
            }
            @keyframes spinner-border-right {
                to {
                    transform: rotate(360deg);                   
                }
            }
            @keyframes spinner-border-left {
                to {
                    transform: rotate(-360deg);
                }
            }    
            @keyframes latido{ 
                0%{
                    opacity: 1.0
                } 
                15%{
                    transform: scale(95%);
                    opacity: 1.0;
                }
                33%{
                    transform: scale(95%);
                    opacity: 1.0;
                }
                100%{
                    opacity: 0.75;
                    transform: scale(90%);
                } 
            }  
            @keyframes parpadeo-latido{                 
                80%{
                    transform: scale(95%)
                } 
                100%{                    
                    transform: scale(100%);
                }
            }
            @keyframes parpadeo-50{ 
                0%{
                    opacity: 1.0;
                } 
                50%{
                    opacity: 0.5;
                } 
                100%{
                    opacity: 1.0;
                }
            }      
             @keyframes brillar{ 
                0%{ 
                    
                    transform: rotate(0deg) scale(67%);
                } 
                50%{      
                    
                    transform: rotate(11.25deg) scale(100%);
                } 
                100%{         
                    
                    transform: rotate(22.5deg) scale(67%);               
                }                
            }   
        </style>        
        <div id="modal-spinner">     
            <div class="spinner-out" ></div>
            <div class="spinner-mid"></div>
            <div class="spinner-in"></div>  
            <div class="spinner-lights" ></div>
            <img class="spinner-logo" src="<?=base_url('img/logo-spinner.png')?>" alt="Espere por favor..." />                      
            <h3 id="modal-spinner-mensaje" class="mensaje latido" ><b>Espere por favor</b></h3>            
        </div>