        <div id="modal-spinner" class="fixed-top bg-dark justify-content-center align-items-center" style="display:none!important; position: fixed; min-width: 100%; min-height: 100%; opacity: 0.75; z-index:5000;">
            <div class="row justify-content-center align-items-center">
                <div class="col col-md-auto ">
                    <div class="row justify-content-center align-items-center text-secondary">
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.500s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.375s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.250s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.125s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem;"></div>
                    </div>
                </div>
                <div class="col-12 col-md-auto pb-2">
                    <div class="row text-secondary text-center"><h1>Espere por favor...</h1></div>
                </div>
                <div class="col col-md-auto">
                    <div class="row justify-content-center align-items-center text-secondary">
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.125s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.25s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.375s;"></div>
                        <div class="col-auto spinner-grow m-2" style="width: 1.5rem; height: 1.5rem; animation-delay: 0.500s;"></div>
                    </div>   
                </div>                        
            </div>           
        </div>
