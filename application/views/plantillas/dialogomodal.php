<div class="modal" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-dark bg-gradient rounded-10 border border-secondary  shadow">
            <div class="modal-header bg-dark bg-gradient text-orange rounded-10 border-0" >
                <h5 class="modal-title" id="staticBackdropLabel"><?= $titulo ?></h5>
                <button type="button" class="btn btn-dark text-orange border border-secondary  d-flex justify-content-center align-items-center" onclick="msgboxDialogResult = 'CANCELAR'; modal.hide();"><i class="fa fa-times fa-fw"></i></button>
            </div>
            <div class="modal-body bg-dark bg-gradient text-white" >
                <div class="row">  
                    <div class="col-auto m-1"><i class="<?= $icono ?>"></i></div>
                    <div class="col m-1 align-items-center d-flex"><div class="p-0 m-0"><?= $mensaje ?></div></div>
                </div>
            </div>
            <div class="modal-footer justify-content-center bg-dark bg-gradient rounded-10 border-0">
                <?php
                if (isset($botones)) {
                    foreach ($botones as $index => $boton) {
                        switch ($boton) {
                            case 'ACEPTAR':
                                ?><button autofocus type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = 'ACEPTAR'; modal.hide();" id="msgbox_btn_ACEPTAR" >Aceptar</button><?php
                                break;
                            case 'CANCELAR':
                                ?><button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = 'CANCELAR';  modal.hide();" id="msgbox_btn_CANCELAR">Cancelar</button><?php
                                break;
                            case 'SI':
                                ?><button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = 'SI';  modal.hide();" id="msgbox_btn_SI">Sí</button><?php
                                break;
                            case 'NO':
                                ?><button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = 'NO';  modal.hide();" id="msgbox_btn_NO">No</button><?php
                                break;
                            default:
                                ?><button type="button" class="btn btn-dark text-orange border border-secondary"  style="min-width: 100px;" onclick="msgboxDialogResult = 'ACEPTAR';  modal.hide();" id="msgbox_btn_ACEPTAR">Aceptar</button><?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
<script id="autoborrar">
    setTimeout( function () {
                    $("#msgbox_btn_<?= $enfocado ?>").focus();        
                    $("#autoborrar").remove();
                },60);
    
</script>  