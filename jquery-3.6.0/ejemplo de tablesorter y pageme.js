
//este codigo va en la vista en donde esta la tabla
setTimeout(function(){
                    $('#cuerpoTabla').pageMe(
                                {
                                    pagerSelector:'#myPager',
                                    showPrevNext:true,
                                    showFirstLast:true,
                                    hidePageNumbers:true,
                                    perPage:14,
                                    childSelector:"tr:not([style='display:none'])"                                    
                                }       
                            );
                    activarToolTips();
                    
                    //setTableExportCellDataTypes();
                    
                    $('.code-hide').remove();
                    var configuracion={
                        theme : "bootstrap",
                        headerTemplate:'<div class="row justify-content-center align-items-center p-0 m-0"> <div class="col-auto p-0 m-0 ms-1 me-1">{content}</div> <div class="col-auto p-0 m-0"> {icon}</div></div>',
//                        cssIcon:"fas fa-sort",
                        cssIconAsc:"fad fa-sort-up fa-2x",
                        cssIconDesc:"fad fa-sort-down fa-2x",
//                        cssIconDisabled:"",
//                        cssIconNone:"",
                        headers:{
                            '.noSort':{
                                sorter:false
                            }
                        }
                    };                    
                    $('#tblConsulta').tablesorter(configuracion);
                    
                    $("#btnCopiar").click(btnCopiar_click);
                    $('#btnDescargar').click(btnDescargar_click);
                    $('#btnDescargarXLSX').click(btnDescargarXLSX_click);
                    $('#btnDescargarPDF').click(btnDescargarPDF_click);
                    $('#btnDescargarTXT').click(btnDescargarTXT_click);
                    $("#btnFullScreen").click(btnFullScreen_click);
                    $('#btnDescargarXLSX').focus();
                },1);