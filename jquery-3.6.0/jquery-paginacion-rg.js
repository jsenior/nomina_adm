/* 
 * 
 * @author: Ing. Raul Gonzalez rauljgf@gmail.com
 * @version: 1.1
 * @copyright: 2022
 * 
 */
jQuery.fn.pageMe = function(opts){
        var $this = this,
            defaults = {
                perPage: 7,
                showPrevNext: false,
                showFirstLast: false,
                hidePageNumbers: false
            },
            settings = $.extend(defaults, opts);

        var listElement = $this;
        var perPage = settings.perPage; 
        var children = listElement.children();
        var pager = $('.pager');
       
        if (typeof settings.childSelector!="undefined") {
            children = listElement.find(settings.childSelector);
        }

        if (typeof settings.pagerSelector!="undefined") {
            pager = $(settings.pagerSelector);
        }
        if (typeof settings.filter!="undefined") {
            $(children).filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(settings.filter) > -1); 
            });            
            children=listElement.find("tr:not(:hidden)");            
        }
        var numItems = children.length;//.size();
        var numPages = Math.ceil(numItems/perPage);

        pager.data("curr",1);
        
        if (settings.showFirstLast){
            $('<li class="page-item first_link" title="Primera página" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"><a href="#" class="page-link first_link"><i class="fa fa-step-backward"></i></a></li>').appendTo(pager);
        }

        if (settings.showPrevNext){
            $('<li class="page-item prev_link" title="Página previa" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"><a href="#" class="page-link prev_link"><i class="fa fa-backward"></i></a></li>').appendTo(pager);
        }

        var curr = 0;
        while(numPages > curr && (settings.hidePageNumbers==false)){
            $('<li class="page-item" title="Página '+(curr+1)+'" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"><a href="#" class="page-link page_link">'+(curr+1)+'</a></li>').appendTo(pager);
            curr++;
        }
        if(settings.hidePageNumbers==true && numItems>0){//ESCONDE LOS NUMEROS EN UNA LISTA SELECT
            $('<select id="pagesSelect" class="form-select form-select-sm w-auto" title="Página Actual" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"></select>').appendTo(pager);
            var pagesSelect=$("#pagesSelect");
            while(numPages > curr && (settings.hidePageNumbers==true)){
                if(curr===0){
                    $('<option value="'+(curr+1)+'" >'+(curr+1)+' de '+numPages+'</option>').appendTo(pagesSelect);   
                }else{
                    $('<option value="'+(curr+1)+'" >'+(curr+1)+'</option>').appendTo(pagesSelect);                
                    
                }
                curr++;
            }            
        }
        

        if (settings.showPrevNext){
            $('<li class="page-item next_link " title="Próxima página" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"><a href="#" class="page-link next_link"><i class="fa fa-forward"></i></a></li>').appendTo(pager);
        }
        if (settings.showFirstLast){
            $('<li class="page-item last_link" title="Última página" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover"><a href="#" class="page-link last_link"><i class="fa fa-step-forward"></i></a></li>').appendTo(pager);
        }
        
        //pager.find('.page_link:first').addClass('active');
        $(".page_link").parents("li").eq(numPages-1).addClass("active");
        //pager.find('.prev_link').hide();
        $(pager.find('.first_link')).addClass("disabled");
        $(pager.find('.prev_link')).addClass("disabled");
        if (numPages<=1) {
            //pager.find('.next_link').hide();
            $(pager.find('.next_link')).addClass("disabled");
            $(pager.find('.last_link')).addClass("disabled");
        }
            //pager.children().eq(1).addClass("active");
            //pager.children().eq(1).addClass("active");

        children.hide();
        children.slice(0, perPage).show();

        pager.find('li .page_link').click(function(){
            //alert($(this).html().valueOf());
            var clickedPage = $(this).html().valueOf();
            goTo(clickedPage,perPage);
            return false;
        });
        pager.find('li .prev_link').click(function(){
            if($(this).hasClass("disabled")){return;}//evita que se produzca el evento SI TIENE EL FOCO Y SE PRESIONA ENTER aunque tenga la clase disabled
            previous();
            return false;
        });
        pager.find('li .next_link').click(function(){
            if($(this).hasClass("disabled")){return;}
            next();
            return false;
        });
        pager.find('li .first_link').click(function(){
            if($(this).hasClass("disabled")){return;}
            goTo(1,perPage);
            return false;
        });
        pager.find('li .last_link').click(function(){
            if($(this).hasClass("disabled")){return;}
            goTo(numPages,perPage);
            return false;
        });
        pager.find('#pagesSelect').change(function(){            
            goTo($(this).val(),perPage);
            return false;
        });
        pager.find('#pagesSelect').focus(function(){            
            goTo($(this).val(),perPage);
            return false;
        });
        function previous(){
            var goToPage = parseInt(pager.data("curr")) - 1;
            goTo(goToPage);
        }

        function next(){
            goToPage = parseInt(pager.data("curr")) + 1;
            goTo(goToPage);
        }

        function goTo(page){
            var startAt = ((page-1) * perPage),
                endOn = startAt + perPage;
            $("#pagesSelect").val(page);
            var opciones=$("#pagesSelect option");
            //alert(opciones.length);
            for(var i=0; i<opciones.length;i++){
                $(opciones[i]).html($(opciones[i]).attr("value"));                
            }
            $("#pagesSelect option[value='"+page+"']").html(page+' de '+numPages);
            children.css('display','none').slice(startAt, endOn).show();

            if (page>=2) {
                //pager.find('.prev_link').show();
                $(pager.find('.prev_link')).removeClass("disabled");
                $(pager.find('.first_link')).removeClass("disabled");
            }else{
                //pager.find('.prev_link').hide();
                $(pager.find('.prev_link')).addClass("disabled");
                
                $(pager.find('.first_link')).addClass("disabled");
                
                pager.find(".page_link").parents("li").eq(numPages-page).find("a.page-link").focus();
                
            }

            if (page<(numPages)) {
                //pager.find('.next_link').show();
                $(pager.find('.next_link')).removeClass("disabled");
                $(pager.find('.last_link')).removeClass("disabled");
            }else{
                //pager.find('.next_link').hide();
                $(pager.find('.next_link')).addClass("disabled");
                
                $(pager.find('.last_link')).addClass("disabled");
                
                pager.find(".page_link").parents("li").eq(numPages-page).find("a.page-link").focus();
            }

            pager.data("curr",page);
            pager.children().removeClass("active");            
            pager.find(".page_link").parents("li").eq(numPages-page).addClass("active");
            
        }
    };

//$(document).ready(function(){
//    
//  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:4});
//    
//});

